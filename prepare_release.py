# Release preparation script for the Zaber Core C# library.
# Edit the version number and version history in README.md if appropriate before running this script.

# This script applies the version number to all relevant files, then compiles
# the library and help text and stages the built files for commit to revision
# control and for publication. It does not perform any actual publication.

import glob
import os
import re
import shutil
import subprocess
import string
import zipfile

# Combines the version number from version.txt with the current revision from git.
def get_version():
    with open("version.txt") as fp:
        lines = list(fp)
        version = str(lines[0]).strip()

    parts = version.split('.')
    if len(parts) > 4:
        parts = parts[0:3]
    else:
        while len(parts) < 4:
            parts.append("0")

    proc = subprocess.Popen(["git", "show-current-rev"], stdout=subprocess.PIPE)
    outs, errs = proc.communicate()
    if proc.returncode != 0:
        raise OSError("git call failed: " + errs.decode("UTF-8"));

    parts[3] = outs.decode("UTF-8").strip()

    version = ""
    for num in parts:
        if len(version) > 0:
            version = version + "."

        val = int(-1)
        try: 
            val = int(num)
        except ValueError:
            pass

        if val == -1:
            raise ValueError("Error: Could not parse version number component '" + num + "'")
            exit(1)

        version = version + str(val)

    if len(version) < 7:
        raise ValueError("Error constructing version number: '" + version + "' is too short.")
        exit(1)

    return version


def regexp_replace_in_file(aFilePath, aPattern, aReplacement, aCount):
    with open(aFilePath) as fp:
        text = fp.read()

    r = re.compile(aPattern)
    if aCount is None:
        text = r.sub(aReplacement, text)
    else:
        text = r.sub(aReplacement, text, count=aCount)

    with open(aFilePath, "wt") as fp:
        fp.write(text)        
    

def msbuild_solution(aFilePath, aConfiguration = "Release", aTargets = "Clean,Build"):

    msbuild = r"C:\Program Files (x86)\MSBuild\14.0\Bin\MsBuild.exe"
    if not os.path.isfile(msbuild):
        msbuild = r"C:\Program Files\MSBuild\14.0\Bin\MsBuild.exe"
    if not os.path.isfile(msbuild):
        raise LookupError("Could not locate MsBuild.exe.")
    
    command = [msbuild, "/nologo"]
    command.append("/property:Configuration=" + aConfiguration)
    command.append("/target:" + aTargets)
    command.append(aFilePath)
    proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    outs, errs = proc.communicate()
    if proc.returncode != 0:
        raise OSError("Compile failed: " + errs.decode("UTF-8"));
    print(outs.decode("UTF-8"))


def doxygen(aDoxyFilePath):
    dir, file = os.path.split(aDoxyFilePath)
    curdir = os.getcwd()
    if os.path.isdir(dir):
        os.chdir(dir)

    proc = subprocess.Popen(["doxygen", file], stdout=subprocess.PIPE)
    outs, errs = proc.communicate()
    os.chdir(curdir)

    if proc.returncode != 0:
        raise OSError("Doc build failed: " + errs.decode("UTF-8"));
    print(outs.decode("UTF-8"))
    return os.path.join(dir, "html")
    

if "__main__" == __name__:

    # Get the version number.
    version = get_version()
    print("Updating versions to " + version + "...")

    # Update the version number in relevant files.
    print("- README.md")
    regexp_replace_in_file("README.md", "\d+\.\d+[\.\d+]*", version, 1)
    print("- Zaber.Serial.Core\\Doxyfile")
    regexp_replace_in_file("Zaber.Serial.Core\\Doxyfile", "PROJECT_NUMBER\s+=\s+\d+\.\d+[\.\d+]*", "PROJECT_NUMBER = " + version, 1)
    for name in glob.iglob("**/AssemblyInfo.cs", recursive=True):
        print("- " + name)
        regexp_replace_in_file(name, "AssemblyVersion\(\"\d+[\.\d+]*\"\)", "AssemblyVersion(\"" + version + "\")", 1)
        regexp_replace_in_file(name, "AssemblyFileVersion\(\"\d+[\.\d+]*\"\)", "AssemblyFileVersion(\"" + version + "\")", 1)

    # Compile the solution, stage binaries and make zip.
    print("Compiling...")
    msbuild_solution("Zaber.Serial.sln")

    print("Staging binaries...")
    binFiles = [r"Zaber.Serial.Core\bin\Release\Zaber.Serial.Core.dll",
	            r"Zaber.Serial.Core\bin\Release\Zaber.Serial.Core.pdb",
	            r"Zaber.Serial.Core\bin\Release\Zaber.Serial.Core.XML"]
    zipFiles = binFiles + ["LICENSE.md", "README.md"]

    zipName = "zaber-core-serial-csharp-v" + version + ".zip"
    with zipfile.ZipFile(zipName, "w") as zip:
        for file in zipFiles:
            zip.write(file, os.path.split(file)[1])
    print("- " + zipName)

    # Build and stage documentation.
    print("Building documentation...")
    docdir = doxygen(r"Zaber.Serial.Core\Doxyfile")
    print("Documentation output is in: " + docdir)

    print("Done!")

    
