﻿using System;
using System.Collections.Generic;
using System.IO;
using Zaber.Serial.Core;

namespace Zaber.Serial.Examples.ConfigUtil
{
	public class DeviceDetector
	{
		public static List<ZaberAsciiDevice> DetectAsciiDevices(IEnumerable<string> aPortList)
		{
			var devices = new List<ZaberAsciiDevice>();

			foreach (string portName in aPortList)
			{
				try
				{
					var asciiPort = new ZaberAsciiPort(portName);
					asciiPort.Open();
                    // Clear any leftover data in the serial port.
                    asciiPort.Drain();

					var devicesFound = asciiPort.FindDevices();
					asciiPort.Close();

					if (devicesFound.Count > 0)
					{
						devices.AddRange(devicesFound.ToArray());
					}
				}
				catch (IOException) { }
				catch (UnauthorizedAccessException) { }
			}

			return devices;
		}


		public static List<ZaberBinaryDevice> DetectBinaryDevices(IEnumerable<string> aPortList)
		{
			var devices = new List<ZaberBinaryDevice>();

			foreach (string portName in aPortList)
			{
				try
				{
					var binaryPort = new ZaberBinaryPort(portName);
                    binaryPort.Open();
                    // Clear any leftover data in the serial port.
                    binaryPort.Drain();

                    foreach (var axis in binaryPort.FindAxes())
					{
						devices.Add(axis as ZaberBinaryDevice);
					}

					binaryPort.Close();
				}
				catch (IOException) { }
				catch (UnauthorizedAccessException) { }
 			}

			return devices;
		}
	}
}
