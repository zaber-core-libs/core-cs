﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Zaber.Serial.Core;

namespace Zaber.Serial.Examples.ConfigUtil
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

			// Populate the list of available serial ports.
			var ports = System.IO.Ports.SerialPort.GetPortNames().ToList();
			ports.Sort();
			_portList.Items.AddRange(ports.ToArray());

			// Pre-select all ports by default.
			for (int i = 0; i < ports.Count; i++)
			{
				_portList.SelectedIndices.Add(i);
			}
		}


		private void detectDevicesButton_Click(object sender, EventArgs e)
		{
			// Confirm detect operation with a dialog box.
			_detectDevicesButton.Enabled = false;
			if (sender == _detectDevicesButton)
			{
				var dialogResult = MessageBox.Show("This will send "
				+ "messages to every selected COM port. If you have other equipment "
				+ "connected to your computer, these messages may result in "
				+ "unexpected behaviour from that equipment! Zaber is not "
				+ "responsible for any damage to or caused by other equipment.",
				"Are you sure?", 
					MessageBoxButtons.YesNo);

				// abort if they say no.
				if (DialogResult.No == dialogResult)
				{
                    UpdateDetectButton();
                    return;
				}
			}

			_changeToAsciiButton.Enabled = false;
			_changeToBinaryButton.Enabled = false;
			_detectDevicesButton.Text = "Detecting...";

			_allDevices.Clear();
			_selectedDevices.Clear();
			_devicesGridView.Rows.Clear();

			// Get the list of user-selected port names.
			var portNames = new List<string>();
			foreach (var selection in _portList.SelectedItems)
			{
				var name = selection as string;
				if (null != name)
				{
					portNames.Add(name);
				}
			}

			// Detect binary devices first because it's easier for an ASCII command to
			// be mistaken for a random binary command than vice versa.
			var binaryDevices = DeviceDetector.DetectBinaryDevices(portNames).ToArray();

			int rowIndex = 0;
			for (int i = 0; i < binaryDevices.Length; i++)
			{
				var device = binaryDevices[i];
				_devicesGridView.Rows.Add();
				_devicesGridView.Rows[rowIndex].Cells["ComPort"].Value = device.Port.PortName;
				_devicesGridView.Rows[rowIndex].Cells["DeviceNumber"].Value = device.Number;
				_devicesGridView.Rows[rowIndex].Cells["Protocol"].Value = "Binary";
				_allDevices.Add(new Tuple<string, byte>(device.Port.PortName, device.Number),
								device);
				rowIndex++;

				// Since we found binary devices on this port, we won't also find ASCII devices.
				if (portNames.Contains(device.Port.PortName))
				{
					portNames.Remove(device.Port.PortName);
				}
			}

			// Now detect ASCII devices on any remaining ports.
			var asciiDevices = DeviceDetector.DetectAsciiDevices(portNames).ToArray();

			for (int i = 0; i < asciiDevices.Length; i++)
			{
				var device = asciiDevices[i];
				_devicesGridView.Rows.Add();
				_devicesGridView.Rows[rowIndex].Cells["ComPort"].Value = device.Port.PortName;
				_devicesGridView.Rows[rowIndex].Cells["DeviceNumber"].Value = device.Address;
				_devicesGridView.Rows[rowIndex].Cells["Protocol"].Value = "ASCII";
				_allDevices.Add(new Tuple<string, byte>(device.Port.PortName, device.Address),
								device);
				rowIndex++;
			}

			_detectDevicesButton.Text = "Detect Devices";
            UpdateDetectButton();
        }


        private void changeToAsciiButton_Click(object sender, EventArgs e)
		{
			// Attempt to switch all selected devices to ASCII mode.
			// This ignores errors; success will be judged by the updated display.
			foreach (var selection in _selectedDevices)
			{
				var device = selection as ZaberBinaryDevice;
				if (null != device)
				{
					var port = device.Port as ZaberBinaryPort;
					port.Open();
					port.Write(new BinaryCommand(device.Number, 124, 115200));
                    // Read and discard the response so it doesn't sit around in the receive buffer.
                    port.Read();
					port.Close();
				}
			}

            // Delay to give the devices time to relinitialize.
            System.Threading.Thread.Sleep(250);

			// Re-detect devices to refresh the list.
			detectDevicesButton_Click(sender, e);
		}


		private void changeToBinaryButton_Click(object sender, EventArgs e)
		{
			// Attempt to switch all selected devices to binary mode.
			// This ignores errors; success will be judged by the updated display.
			foreach (var selection in _selectedDevices)
			{
				byte address = 0;
				IZaberPort port = null;
				var device = selection as ZaberAsciiDevice;
				if (null != device)
				{
					port = device.Port;
					address = device.Address;
				}
				else
				{
					var axis = selection as ZaberAsciiAxis;
					if (null != axis)
					{
						port = axis.Port;
						address = axis.ParentAddress;
					}
				}

				if (null != port)
				{
					port.Open();
					port.Write(new AsciiCommand(device.Address, 0, "tools setcomm 9600 1"));
                    // Read and discard the response so it doesn't sit around in the receive buffer.
                    port.Read();
                    port.Close();
				}
			}

            // Delay to give the devices time to relinitialize.
            System.Threading.Thread.Sleep(250);

            // Re-detect devices to refresh the list.
            detectDevicesButton_Click(sender, e);
		}


		// The WinForms GridView doesn't provide an easy way to detect state changes
		// of checkboxes in a column, so we just re-scan the whole list whenever 
		// a cell content item is clicked on.
		private void devicesGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
		{
			_devicesGridView.CommitEdit(DataGridViewDataErrorContexts.Commit);

			for (int i = 0; i < _devicesGridView.Rows.Count; i++)
			{
				var row = _devicesGridView.Rows[i];
				var portName = row.Cells["ComPort"].Value as string;
				var address = (byte)row.Cells["DeviceNumber"].Value;
				var selected = Convert.ToBoolean(row.Cells["Selection"].Value);
				var key = new Tuple<string, byte>(portName, address);
				var device = _allDevices[key];
				if (selected)
				{
					_selectedDevices.Add(device);
				}
				else
				{
					_selectedDevices.Remove(device);
				}
			}

			UpdateConvertButtons();
		}


		private void UpdateConvertButtons()
		{
			var hasBinary = false;
			var hasAscii = false;

			foreach (var device in _selectedDevices)
			{
				hasBinary |= (device is ZaberBinaryDevice);
				hasAscii |= (device is ZaberAsciiDevice);
				hasAscii |= (device is ZaberAsciiAxis);
			}

			_changeToAsciiButton.Enabled = hasBinary;
			_changeToBinaryButton.Enabled = hasAscii;
		}


		private void _portList_SelectedIndexChanged(object sender, EventArgs e)
		{
            UpdateDetectButton();
		}


        private void UpdateDetectButton()
        {
            _detectDevicesButton.Enabled = (_portList.SelectedItems.Count > 0);
        }


        private Dictionary<Tuple<string, byte>, object> _allDevices = new Dictionary<Tuple<string, byte>, object>();
		private HashSet<object> _selectedDevices = new HashSet<object>();
	}
}
