﻿namespace Zaber.Serial.Examples.ConfigUtil
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
			this._detectDevicesButton = new System.Windows.Forms.Button();
			this._changeToAsciiButton = new System.Windows.Forms.Button();
			this._changeToBinaryButton = new System.Windows.Forms.Button();
			this._devicesGridView = new System.Windows.Forms.DataGridView();
			this.Selection = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.ComPort = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.DeviceNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.Protocol = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.label1 = new System.Windows.Forms.Label();
			this._portList = new System.Windows.Forms.ListBox();
			((System.ComponentModel.ISupportInitialize)(this._devicesGridView)).BeginInit();
			this.SuspendLayout();
			// 
			// _detectDevicesButton
			// 
			this._detectDevicesButton.Location = new System.Drawing.Point(165, 48);
			this._detectDevicesButton.Name = "_detectDevicesButton";
			this._detectDevicesButton.Size = new System.Drawing.Size(111, 37);
			this._detectDevicesButton.TabIndex = 1;
			this._detectDevicesButton.Text = "Detect Devices on Selected Ports";
			this._detectDevicesButton.UseVisualStyleBackColor = true;
			this._detectDevicesButton.Click += new System.EventHandler(this.detectDevicesButton_Click);
			// 
			// _changeToAsciiButton
			// 
			this._changeToAsciiButton.Enabled = false;
			this._changeToAsciiButton.Location = new System.Drawing.Point(424, 186);
			this._changeToAsciiButton.Name = "_changeToAsciiButton";
			this._changeToAsciiButton.Size = new System.Drawing.Size(111, 40);
			this._changeToAsciiButton.TabIndex = 2;
			this._changeToAsciiButton.Text = "Change Selected Devices to ASCII";
			this._changeToAsciiButton.UseVisualStyleBackColor = true;
			this._changeToAsciiButton.Click += new System.EventHandler(this.changeToAsciiButton_Click);
			// 
			// _changeToBinaryButton
			// 
			this._changeToBinaryButton.Enabled = false;
			this._changeToBinaryButton.Location = new System.Drawing.Point(424, 244);
			this._changeToBinaryButton.Name = "_changeToBinaryButton";
			this._changeToBinaryButton.Size = new System.Drawing.Size(111, 42);
			this._changeToBinaryButton.TabIndex = 3;
			this._changeToBinaryButton.Text = "Change Selected Devices to Binary";
			this._changeToBinaryButton.UseVisualStyleBackColor = true;
			this._changeToBinaryButton.Click += new System.EventHandler(this.changeToBinaryButton_Click);
			// 
			// _devicesGridView
			// 
			this._devicesGridView.AllowUserToAddRows = false;
			this._devicesGridView.AllowUserToDeleteRows = false;
			this._devicesGridView.AllowUserToResizeRows = false;
			this._devicesGridView.BackgroundColor = System.Drawing.SystemColors.Window;
			this._devicesGridView.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
			this._devicesGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
			dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
			dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight;
			dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
			dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
			dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
			dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
			this._devicesGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this._devicesGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this._devicesGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Selection,
            this.ComPort,
            this.DeviceNumber,
            this.Protocol});
			this._devicesGridView.GridColor = System.Drawing.SystemColors.ControlDarkDark;
			this._devicesGridView.Location = new System.Drawing.Point(15, 113);
			this._devicesGridView.Name = "_devicesGridView";
			this._devicesGridView.RowHeadersVisible = false;
			this._devicesGridView.Size = new System.Drawing.Size(403, 239);
			this._devicesGridView.TabIndex = 4;
			this._devicesGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.devicesGridView_CellContentClick);
			// 
			// Selection
			// 
			this.Selection.FalseValue = "false";
			this.Selection.HeaderText = "Selected";
			this.Selection.Name = "Selection";
			this.Selection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.Selection.TrueValue = "true";
			// 
			// ComPort
			// 
			this.ComPort.HeaderText = "COM Port";
			this.ComPort.Name = "ComPort";
			this.ComPort.ReadOnly = true;
			// 
			// DeviceNumber
			// 
			this.DeviceNumber.HeaderText = "Device Number";
			this.DeviceNumber.Name = "DeviceNumber";
			this.DeviceNumber.ReadOnly = true;
			// 
			// Protocol
			// 
			this.Protocol.HeaderText = "Protocol";
			this.Protocol.Name = "Protocol";
			this.Protocol.ReadOnly = true;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(138, 13);
			this.label1.TabIndex = 5;
			this.label1.Text = "Select serial ports to check:";
			// 
			// _portList
			// 
			this._portList.FormattingEnabled = true;
			this._portList.Location = new System.Drawing.Point(15, 25);
			this._portList.Name = "_portList";
			this._portList.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this._portList.Size = new System.Drawing.Size(144, 82);
			this._portList.TabIndex = 6;
			this._portList.SelectedIndexChanged += new System.EventHandler(this._portList_SelectedIndexChanged);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(549, 372);
			this.Controls.Add(this._portList);
			this.Controls.Add(this.label1);
			this.Controls.Add(this._devicesGridView);
			this.Controls.Add(this._changeToBinaryButton);
			this.Controls.Add(this._changeToAsciiButton);
			this.Controls.Add(this._detectDevicesButton);
			this.Name = "MainForm";
			this.Text = "   ";
			((System.ComponentModel.ISupportInitialize)(this._devicesGridView)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button _detectDevicesButton;
        private System.Windows.Forms.Button _changeToAsciiButton;
        private System.Windows.Forms.Button _changeToBinaryButton;
        private System.Windows.Forms.DataGridView _devicesGridView;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Selection;
		private System.Windows.Forms.DataGridViewTextBoxColumn ComPort;
		private System.Windows.Forms.DataGridViewTextBoxColumn DeviceNumber;
		private System.Windows.Forms.DataGridViewTextBoxColumn Protocol;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ListBox _portList;
	}
}

