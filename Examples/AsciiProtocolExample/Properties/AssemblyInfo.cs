﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Zaber Core Serial ASCII Protocol Example")]
[assembly: AssemblyDescription("Getting started sample program for the Zaber C# Core Serial library.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Zaber Technologies Inc.")]
[assembly: AssemblyProduct("Zaber.Serial.Core.Examples.AsciiProtocolExample")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("92ab4228-4afa-4e66-9551-b8ca59f1e288")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.3.0.102")]
[assembly: AssemblyFileVersion("1.3.0.102")]
