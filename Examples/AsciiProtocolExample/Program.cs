﻿using System;
using System.Linq;
using System.Threading;

namespace Zaber.Serial.Core.Examples.AsciiProtocol
{
	/// <summary>
	/// This is a short demo program showing how to use the Zaber C# Core Serial
	/// library with the Zaber ASCII protocol. This program assumes you have already
	/// configured one or more Zaber devices to use the ASCII protocol on one of
	/// your computer's serial ports. By default it will look for devices at 115,200 baud
	/// on COM1, but you can override that by putting the serial port name and baud rate
	/// on the command line, in that order, or by changing the constants below and 
	/// recompiling the program.
	/// 
	/// This program does no error handling so if the port name is wrong
	/// you will get an ugly message in the console output.
	/// </summary>
	public class Program
	{
		const string DEFAULT_PORT_NAME = "COM1";
		const int DEFAULT_BAUD_RATE = 115200;
		const int DEFAULT_SPEED = 5000;

		static void Main(string[] args)
		{
			var portName = DEFAULT_PORT_NAME;
			var baudRate = DEFAULT_BAUD_RATE;

			if (args.Length > 1)
			{
				baudRate = int.Parse(args[1]);
			}

			if (args.Length > 0)
			{
				portName = args[0];
			}


			Console.WriteLine("Opening port {0} at {1} baud.", portName, baudRate);

			var port = new ZaberAsciiPort(portName, baudRate);
			port.Open();

			// Find all axes on this port. Note the FindAxes method checks system.axiscount
			// on each device and creates the right number of axes for the device; the actual
			// number of devices may be less.
			var axes = port.FindAxes().Select(d => d as ZaberAsciiAxis).Where(d => null != d).ToList();

			if (axes.Count > 0)
			{
				Console.WriteLine("Found device(s) at address {0}", string.Join(", ", axes.Select(a => $"({ a.ParentAddress } { a.Number })")));
				Console.Write("OK to make the devices move? (y/N) ");
				var key = Console.ReadKey().Key;
				Console.WriteLine();

				if (ConsoleKey.Y == key)
				{
					foreach (var axis in axes)
					{
						// MOVE 1: Home the device to make sure it has its positon reference.
						Console.WriteLine("Moving device {0}:", axis.Number);
						Console.WriteLine("- Homing");
						axis.Home();

						// MOVE 2: Move forward at maximum speed for two seconds, then stop and report position.

						// Read the device's target speed setting to use with the MoveVelocity command below.
						axis.Port.Write(new AsciiCommand(axis, "get maxspeed"));
						decimal speed = DEFAULT_SPEED;
						var reply = axis.Port.Read() as AsciiReply;
						// A command code of 255 in the reply means there was an error.
						if ((null != reply) && (!reply.Rejected))
						{
							speed = decimal.Parse(reply.ResponseData);
						}

						Console.WriteLine("- Moving at speed {0} for 2 seconds", speed);
						axis.MoveVelocity(speed);
						Thread.Sleep(2000);
						axis.Stop();

						// Read the device's position and report it.
						axis.Port.Write(new AsciiCommand(axis, "get pos"));
						reply = axis.Port.Read() as AsciiReply;
						if ((null != reply) && (!reply.Rejected))
						{
							Console.WriteLine("- Current position is {0}", reply.ResponseData);
						}
						else
						{
							Console.WriteLine("- Failed to read the device's current position.");
						}


						// MOVE 3: Return to the home position again.
						Console.WriteLine("- Homing device {0} again", axis.Number);
						axis.Home();
					}
				}
			}
			else
			{
				Console.WriteLine("No devices were detected.");
			}

			Console.WriteLine("Closing the port.");
			port.Close();
		}
	}
}
