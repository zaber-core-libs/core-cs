﻿using System;
using System.Linq;
using System.Threading;

namespace Zaber.Serial.Core.Examples.BinaryProtocol
{
	/// <summary>
	/// This is a short demo program showing how to use the Zaber C# Core Serial
	/// library with the Zaber Binary protocol. This program assumes you have already
	/// configured one or more Zaber devices to use the Binary protocol on one of
	/// your computer's serial ports. By default it will look for devices at 9600 baud
	/// on COM1, but you can override that by putting the serial port name and baud rate
	/// on the command line, in that order, or by changing the constants below and 
	/// recompiling the program.
	/// This program does no error handling so if the port name is wrong
	/// you will get an ugly message in the console output.
	/// </summary>
	public class Program
	{
		const string DEFAULT_PORT_NAME = "COM1";
		const int DEFAULT_BAUD_RATE = 9600;
		const int DEFAULT_SPEED = 5000; 

		static void Main(string[] args)
		{
			var portName = DEFAULT_PORT_NAME;
			var baudRate = DEFAULT_BAUD_RATE;

			if (args.Length > 1)
			{
				baudRate = int.Parse(args[1]);
			}

			if (args.Length > 0)
			{
				portName = args[0];
			}


			Console.WriteLine("Opening port {0} at {1} baud.", portName, baudRate);

			var port = new ZaberBinaryPort(portName, baudRate);
			port.Open();

			var devices = port.FindAxes().Select(d => d as ZaberBinaryDevice).Where(d => null != d).ToList();
			if (devices.Count > 0)
			{
				Console.WriteLine("Found device(s) at address {0}", string.Join(", ", devices.Select(d => d.Number)));
				Console.Write("OK to make the devices move? (y/N) ");
				var key = Console.ReadKey().Key;
				Console.WriteLine();

				if (ConsoleKey.Y == key)
				{
					foreach (var device in devices)
					{
						// MOVE 1: Home the device to make sure it has its positon reference.
						Console.WriteLine("Moving device {0}:", device.Number);
						Console.WriteLine("- Homing");
						device.Home();

						// MOVE 2: Move forward at maximum speed for two seconds, then stop and report position.

						// Read the device's target speed setting to use with the MoveVelocity command below.
						// Command 53 returns the value of a setting, and setting 42 is the target speed.
						device.Port.Write(new BinaryCommand(device.Number, 53, 42));
						int speed = DEFAULT_SPEED;
						var reply = device.Port.Read() as BinaryReply;
						// A command code of 255 in the reply means there was an error.
						if ((null != reply) && (255 != reply.CommandNumber))
						{
							speed = reply.Data;
						}

						Console.WriteLine("- Moving at speed {0} for 2 seconds", speed);
						device.MoveVelocity(speed);
						Thread.Sleep(2000);
						device.Stop();

						// Read the device's position and report it.
						// Command 60 returns the current position. Data value is ignored.
						device.Port.Write(new BinaryCommand(device.Number, 60, 0));
						reply = device.Port.Read() as BinaryReply;
						if ((null != reply) && (255 != reply.CommandNumber))
						{
							Console.WriteLine("- Current position is {0}", reply.Data);
						}
						else
						{
							Console.WriteLine("- Failed to read the device's current position.");
						}


						// MOVE 3: Return to the home position again.
						Console.WriteLine("- Homing device {0} again", device.Number);
						device.Home();
					}
				}
			}
			else
			{
				Console.WriteLine("No devices were detected.");
			}

			Console.WriteLine("Closing the port.");
			port.Close();
		}
	}
}
