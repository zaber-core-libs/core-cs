# The Zaber Core Serial Library in C&#35;

**Version 1.4**

Thanks for choosing to use a Zaber device! This library is intended to help 
a software project in any .NET language communicate with Zaber devices quickly
and easily. Though the library was written in C#, it is distributed as a
compiled DLL which can be used from any .NET language. In addition, this
package contains a program database (pdb) file for debugging, and in-line
documentation in XML for Visual Studio's IntelliSense. 

## Deprecation notice

Development of this library is being discontinued in favor of the new
Zaber Motion Library (for the [ASCII Protocol](https://www.zaber.com/software/docs/motion-library/ascii/tutorials/install/cs/),
or for the [Binary Protocol](https://www.zaber.com/software/docs/motion-library/binary/tutorials/install/cs/)),
which provides more features, more robust protocol handling, and can be
installed via NuGet. 

Zaber will still support existing users of the Zaber Core Serial Libraries, but no new feature
development is planned. 

## Usage & Documentation

Full documentation and example code can be found online 
at http://www.zaber.com/support/docs/api/core-csharp/.

## Installation

This library is distributed as a DLL, which can be imported into Visual Studio
as a reference like all other DLL libraries. The binary distribution of this
library is available at https://www.zaber.com/zaber-software#libraries.

To import a library into an existing Visual Studio project, open the Solution
Explorer and right-click "References". Choose "Add Reference...", and browse
to the location of the DLL. 

If the .pdb and .xml files provided are in the same directory as the DLL,
they should be automatically detected by Visual Studio. It is recommended that
the library be kept in the same general folder structure as the rest of your
project, in a directory named "lib" or something similar.

## License & Source Code

This library is open-source, and is licensed under the Apache Software License 
Version 2.0. The source code is available at https://gitlab.com/zaber-core-libs/core-cs.

See [LICENSE.md](LICENSE.md) for the full license text.

## Contact Us

If you need to contact Zaber for any reason, please send an email to
contact@zaber.com. More detailed contact information can be found online at
http://www.zaber.com/contact/. Please do not hesitate to ask us for help with
this library or our devices.

## Changelog

To download older versions please see the [Tags page](/../tags).

### v1.4
* Updated .NET Core dependency to version 3.1.

### v1.3
* The signatures of the axis move functions have changed to take decimal arguments
  instead of integers, in order to support the large range of values possible with
  newer Zaber devices. Also the GetPosition() method has been deprecated; a new
  method, GetCurrentPosition(), which returns a decimal instead of an integer, is
  recommended for use in new code.
* The ZaberBinaryPort class now supports baud rates other than 9600. 9600 is the default.
* Some non-breaking interface refactoring has been done to aid testability:
** The IZaberPort interface now has Write and Read methods that work with the 
   Command and Reply base classes respetively. The ZaberAsciiPort and ZaberBinaryPort
   classes implement these new methods explicitly, but the previous protocol-specific
   implementations that work with subclasses of Command and Reply still exist.
** The device and axis classes now use the IZaberPort interface internally. This
   allows automated tests to use a simulated port so no actual serial hardware 
   is needed for testing.
* In addition to .NET Framework, the library now works with .NET Core version 
  2.0 and up (on Windows only, because the Microsoft serial ports preview package currently 
  only supports that platform). A second set of project files starting at 
  Zaber.Serial.Core**_std**.sln can be used to compile the library and two of the 
  examples for .NET Core. The original Zaber.Serial.sln file still targets .NET 
  Framework. Note the .NET Core solution file requires Visual Studio 2017 or later.
* The AsciiReply class will now verify message checksums if they are present, instead
  of blindly including them in the data payload.
* The example programs have been rewritten; there are now two simple examples each
  demonstrating basic use of each protocol, and the GUI utility example has been
  reworked to enable selection of ports rather than always using all ports.
** Note the GUI example (ConfigUtil) does not work with 64-bit builds of Mono, due to 
   incomplete support for Windows Forms in that environment.
* The default build settings of the library project have been changed to treat
  all warnings as errors.
* The tests have been converted to the NUnit testing framework and rewritten to 
  no longer need actual serial devices connected. This was done to facilitate 
  automated testing of the library code.

### v1.2
* Fixed a bug that would cause ASCII Axis commands to throw exceptions if
  a device had Alert messages enabled.

### v1.1
* Fixed a bug where the device number of a BinaryReply would not be set.
* Changed target runtime to 4.0.

### v1.0
* First public source code release.