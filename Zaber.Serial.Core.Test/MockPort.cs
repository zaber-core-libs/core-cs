﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	public class MockPort : IZaberPort
	{
		public object SendLock { get { return _sendLock; } }

		public List<IZaberAxis> AvailableAxes { get; set; }

		/// <summary>
		/// Expect that the Write method will be called with a binary command.
		/// </summary>
		/// <param name="aDeviceNumber">The device number to expect.</param>
		/// <param name="aCommand">The command to expect.</param>
		/// <param name="aData">The data value to expect.</param>
		public void Expect(byte aDeviceNumber, byte aCommand, int aData)
		{
			Expect(new BinaryCommand(aDeviceNumber, aCommand, aData));
		}


		/// <summary>
		/// Expect that the Write method will be called with a text command.
		/// </summary>
		/// <param name="aMessage">The command to expect.</param>
		public void Expect(string aMessage)
		{
			var textToSend =
				aMessage.StartsWith("/", StringComparison.Ordinal)
				? aMessage
				: "/" + aMessage;

			Expect(new AsciiCommand(textToSend));
		}


		/// <summary>
		/// Expect that the Write method will be called with a user-constructed command.
		/// </summary>
		/// <param name="aCommand">The command to expect.</param>
		public void Expect(Command aCommand)
		{
			var expectation = new Expectation
			{
				ExpectedRequest = aCommand
			};

			_expectations.AddLast(expectation);
		}


		/// <summary>
		/// Append a binary response to the last-added expectation.
		/// </summary>
		/// <param name="aDeviceNumber">Source device of the reply.</param>
		/// <param name="aCommand">COmmand field of the reply.</param>
		/// <param name="aData">Data value of the reply.</param>
		public void AddResponse(byte aDeviceNumber, byte aCommand, int aData)
		{
			AddResponse(new BinaryReply(aDeviceNumber, aCommand, aData));
		}


		/// <summary>
		/// Append an ASCII response to the last-added expectation.
		/// </summary>
		/// <param name="aMessage">Reply text.</param>
		public void AddResponse(string aMessage)
		{
			AddResponse(new AsciiReply(aMessage));
		}


		/// <summary>
		/// Append a user-constructed response to the last-added expectation.
		/// </summary>
		/// <param name="aMessage">Reply to queue up.</param>
		public void AddResponse(Reply aMessage)
		{
			_expectations.Last.Value.ResponsePackets.Add(aMessage);
		}


		/// <summary>
		/// Queue up an exception to be thrown when the last-added expectation is reached.
		/// </summary>
		/// <param name="aException">The exception to throw when the expected command is written.</param>
		public void AddException(Exception aException)
		{
			_expectations.Last.Value.WriteException = aException;
		}


		/// <summary>
		/// Verify that all expectations have occurred.
		/// </summary>
		public void Verify()
		{
			if (_expectations.Count > 0)
			{
				var sb = new StringBuilder();
				sb.AppendLine("Unmatched mock port expectations:");
				foreach (var expectation in _expectations)
				{
					if (null == expectation.ExpectedRequest)
					{
						sb.AppendLine("Read()");
					}
					else
					{
						sb.AppendLine(expectation.ExpectedRequest.ToString());
					}
				}

				Assert.Fail(sb.ToString());
			}

			Assert.AreEqual(0, _unconsumedReplies.Count, "Some replies were not consumed.");
		}


		/// <summary>
		/// Add expectations and responses from a port after reading a text file that is representative
		/// of what appears on the log on Zaber Console after the initial query of a joystick.
		/// </summary> 
		public void ReadExpectationsFromFile(string aFilePath)
		{
			try
			{
				ReadExpectationsFromString(File.ReadAllText(aFilePath));
			}
			catch (Exception aException)
			{
				Assert.Fail("File not found. Please verify the path is correct and the file is marked as content to be copied in the Visual Studio project. Error: " + aException.Message);
			}
		}


		/// <summary>
		/// Add expectations and responses from a string copies for the Zaber Console message log.
		/// </summary>
		public void ReadExpectationsFromString(string aContent)
		{
			using (var reader = new StringReader(aContent))
			{
				string line;

				while ((line = reader.ReadLine()) != null)
				{
					if (line.Length > 0)
					{
						switch (line[0])
						{
							case '/':
								Expect(line);
								break;
							case '@':
								AddResponse(line);
								break;
							case '#':
								AddResponse(line);
								break;
						}
					}
				}
			}
		}

		#region -- IZaberPort implementation --

		public string PortName { get; private set; }

		public bool IsOpen { get; private set; }

		public int ReadTimeout { get; set; }

		public void Open()
		{
			IsOpen = true;
		}

		public void Close()
		{
			IsOpen = false;
		}


		public void Write(Command command)
		{
			if (!_expectations.Any())
			{
				Assert.Fail("Unexpected data packet: {0}", command.ToString());
			}

			var expectation = _expectations.First.Value;
			_expectations.RemoveFirst();

			var expectedCall =
				expectation.ExpectedRequest == null
				? "Read()"
				: expectation.ExpectedRequest.ToString();
			var actualCall = command.ToString();

			Assert.AreEqual(expectedCall, actualCall, "port call");

			if (null != expectation.WriteException)
			{
				throw expectation.WriteException;
			}

			foreach (var reply in expectation.ResponsePackets)
			{
				_unconsumedReplies.Enqueue(reply);
			}
		}


		public Reply Read()
		{
			if (_unconsumedReplies.Any())
			{
				return _unconsumedReplies.Dequeue();
			}

			throw new TimeoutException("There were no queued replies to read.");
		}

		public void Drain()
		{
			_unconsumedReplies.Clear();
		}

		public List<IZaberAxis> FindAxes()
		{
			return AvailableAxes ?? new List<IZaberAxis>();
		}

		#endregion
		#region IDisposable Members

		public void Dispose()
		{
			Close();
		}

		#endregion

		private class Expectation
		{
			public Expectation()
			{
				ResponsePackets = new List<Reply>();
			}

			public Command ExpectedRequest { get; set; }
			public List<Reply> ResponsePackets { get; private set; }
			public Exception WriteException { get; set; }
		}

		private readonly LinkedList<Expectation> _expectations = new LinkedList<Expectation>();
		private readonly object _sendLock = new object();
		private Queue<Reply> _unconsumedReplies = new Queue<Reply>();
	}
}
