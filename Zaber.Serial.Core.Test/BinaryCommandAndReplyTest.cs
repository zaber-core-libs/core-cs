﻿
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(BinaryCommand) + " and " + nameof(BinaryReply) + " classes.")]
	public class BinaryCommandAndReplyTest
	{
		[Test]
		public void TestBinaryCommandEncoding()
		{
			var command = new BinaryCommand(new byte[] { 1, 55, 255, 255, 255, 255 });
			Assert.AreEqual(command.Data, -1);
			Assert.AreEqual(command.DeviceAddress, 1);
			Assert.AreEqual(command.CommandNumber, 55);
		}


		[Test]
		public void TestBinaryCommandEquality()
		{
			BinaryCommand command3 = new BinaryCommand();
			Command command4 = new BinaryCommand(1, 55, 12312);
			Assert.AreNotEqual(command3, command4);
			command3.DeviceAddress = 1;
			command3.CommandNumber = 55;
			command3.Data = 12312;
			Assert.AreEqual(command3, command4);
		}


		[Test]
		public void TestBinaryReplyEquality()
		{
			var reply1 = new BinaryReply();
			var reply2 = new BinaryReply(new byte[] { 1, 55, 12, 222, 59, 6 });
			var reply3 = new BinaryReply(1, 55, 
				(6 << 24 | 59 << 16 | 222 << 8 | 12)); // Should be the same data as reply2.
			reply1.CommandNumber = 55;
			reply1.DeviceAddress = 1;
			reply1.Data = reply3.Data;
			Assert.AreEqual(reply1, reply2);
			Assert.AreEqual(reply1, reply3);
			Assert.AreEqual(reply2, reply3);
		}


		[Test]
		public void TestBinaryReplyParsing()
		{
			var reply = new BinaryReply(new byte[] { 1, 55, 255, 255, 255, 255 });
			Assert.AreEqual(-1, reply.Data);
			Assert.AreEqual(1, reply.DeviceAddress);
			Assert.AreEqual(55, reply.CommandNumber);
		}


		[Test]
		public void TestBinaryCommandToBinaryArrayEncoding()
		{
			var command = new BinaryCommand(1, 55, -1);
			var bytes = command.ToByteArray();
			Assert.AreEqual(1, bytes[0]);
			Assert.AreEqual(55, bytes[1]);
			Assert.AreEqual(255, (bytes[2] & bytes[3] & bytes[4] & bytes[5]) );
		}
	}
}
