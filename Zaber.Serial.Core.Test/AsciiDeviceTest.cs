﻿using System;
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(ZaberAsciiDevice) + " class.")]
	public class AsciiDeviceTest
	{
		[SetUp]
		public void Initialize()
		{
			_port = new MockPort();
			_port.Open();
			_device = new ZaberAsciiDevice(_port, 1);
		}


		[TearDown]
		public void Cleanup()
		{
			_port.Close();
		}


		[Test]
		[Description("Verify that the constructor initializes the port and address properties.")]
		public void PortAndAddress()
		{
			Assert.AreEqual(1, _device.Address);
			Assert.AreEqual(_port, _device.Port);
		}


		[Test]
		[Description("Verify that getting a valid axis returns expected results.")]
		public void GetValidAxis()
		{
			for (int i = 1; i <= 9; ++i)
			{
				var axis = _device.GetAxis(i);
				Assert.AreEqual(i, axis.Number);
				Assert.AreEqual(_device.Address, axis.ParentAddress);
				Assert.AreEqual(_port, axis.Port);
			}
		}


		[Test]
		[Description("Verify that getting an invalid axis throws errors.")]
		public void GetInvalidAxis()
		{
			Assert.Throws<ArgumentException>(() =>
			{
				_device.GetAxis(0);
			});

			Assert.Throws<ArgumentException>(() =>
			{
				_device.GetAxis(10);
			});
		}


		private IZaberPort _port;
		private ZaberAsciiDevice _device;
	}
}
