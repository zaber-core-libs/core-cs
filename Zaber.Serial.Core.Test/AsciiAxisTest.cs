﻿using System;
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(ZaberAsciiAxis) + " class.")]
	public class AsciiAxisTest
	{
		[SetUp]
		public void Initialize()
		{
			_port = new MockPort();
			_port.Open();
			_device = new ZaberAsciiDevice(_port, 1);
			_axis = new ZaberAsciiAxis(_device, 1);
		}


		[TearDown]
		public void Cleanup()
		{
			_port.Close();
			_port.Dispose();
		}


		[Test]
		[Description("Test busy check.")]
		public void TestIsBusy()
		{
			var script =
				"/1 1 \r\n" + // Note CRLFs here are for string splitting in MockPort.
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.IsTrue(_axis.IsBusy());
			Assert.IsFalse(_axis.IsBusy());
		}


		[Test]
		[Description("Test that the home command waits for idle.")]
		public void TestHomeWaitsUntilIdle()
		{
			var script =
				"/1 1 home\r\n" + // Note CRLFs here are for string splitting in MockPort.
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n";
			_port.ReadExpectationsFromString(script);
			_axis.Home();
			_port.Verify();
		}


		[Test]
		[Description("Test that move rel command waits for idle.")]
		public void TestMultipleMoveRel()
		{
			var script =
				"/1 1 move rel 10000\r\n" + // First move
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n" +
				"/1 1 move rel -10000\r\n" + // Second move
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n" +
				"/1 1 move rel 10000\r\n" + // Third move
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n" +
				"/1 1 move rel -10000\r\n" + // Fourth move
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n";
			_port.ReadExpectationsFromString(script);

			// None of these commands pre-empt each other. The device should move
			// exactly four times.
			_axis.MoveRelative(10000);
			_axis.MoveRelative(-10000);
			_axis.MoveRelative(10000);
			_axis.MoveRelative(-10000);
			_port.Verify();
		}


		[Test]
		[Description("Test that the move abs command waits for idle, with varying wait lengths.")]
		public void TestPollUntilIdle()
		{
			// Try a few different time-to-idle loop lengths.
			for (int i = 1; i < 51; i += 10)
			{
				_port.Expect("/1 1 move abs 1000");
				_port.AddResponse("@01 1 OK BUSY -- 0");

				for (int j = 0; j <= i; ++j)
				{
					_port.Expect("/1 1 ");
					_port.AddResponse("@01 1 OK BUSY -- 0");
				}

				_port.Expect("/1 1 ");
				_port.AddResponse("@01 1 OK IDLE -- 0");
				_axis.MoveAbsolute(1000);
				_port.Verify();
			}
		}


		[Test]
		[Description("Test that the stop command waits for idle.")]
		public void TestStopWaitsUntilIdle()
		{
			var script =
				"/1 1 stop\r\n" + // Note CRLFs here are for string splitting in MockPort.
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK BUSY -- 0\r\n" +
				"/1 1 \r\n" +
				"@01 1 OK IDLE -- 0\r\n";
			_port.ReadExpectationsFromString(script);
			_axis.Stop();
			_port.Verify();
		}


		[Test]
		[Description("Test that the move vel command does not wait for idle.")]
		public void TestMoveVelDoesntWaitUntilIdle()
		{
			var script =
				"/1 1 move vel 1000\r\n" + // Note CRLFs here are for string splitting in MockPort.
				"@01 1 OK BUSY -- 0\r\n";
			_port.ReadExpectationsFromString(script);
			_axis.MoveVelocity(1000);
			_port.Verify();
		}


		[Test]
		[Description("Test getting the device position")]
		public void TestGetCurrentPosition()
		{
			var script =
				"/1 1 get pos\r\n" +
				"@01 1 OK BUSY -- 12345\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.AreEqual(12345m, _axis.GetCurrentPosition());
			_port.Verify();

			script =
				"/1 1 get pos\r\n" +
				"@01 1 OK IDLE -- 54321\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.AreEqual(54321m, _axis.GetCurrentPosition());
			_port.Verify();
		}


		[Test]
		[Description("Test getting the device position, legacy edition")]
		public void TestGetPosition()
		{
			var script =
				"/1 1 get pos\r\n" +
				"@01 1 OK BUSY -- 12345\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.AreEqual(12345, _axis.GetPosition());
			_port.Verify();

			script =
				"/1 1 get pos\r\n" +
				"@01 1 OK IDLE -- 54321\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.AreEqual(54321, _axis.GetPosition());
			_port.Verify();
		}


		[Test]
		[Description("Test that a missing reply propagates to user code.")]
		public void TestNoReply()
		{
			var script = "/1 1 get pos\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.Throws<TimeoutException>(() =>
			{
				_axis.GetCurrentPosition();
			});
		}


		[Test]
		[Description("Test that a rejected reply propagates an error to user code.")]
		public void TestRejectedReply()
		{
			var script =
				"/1 1 get pos\r\n" +
				"@01 1 RJ BADCOMMAND -- 12345\r\n";
			_port.ReadExpectationsFromString(script);
			Assert.Throws<UnexpectedReplyReceivedException>(() =>
			{
				_axis.GetCurrentPosition();
			});
		}


		private MockPort _port;
		private ZaberAsciiDevice _device;
		private ZaberAsciiAxis _axis;
	}
}
