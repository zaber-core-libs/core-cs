﻿using System;
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(AsciiCommand) + " and " + nameof(AsciiReply) + " classes.")]
	public class AsciiCommandAndReplyTest
	{
		[Test]
		public void TestReplyParsing()
		{
			var reply = new AsciiReply("@01 0 OK IDLE -- 0");
			Assert.AreEqual(AsciiReply.ReplyType.Response, reply.MessageType);
			Assert.AreEqual(1, reply.DeviceAddress);
			Assert.AreEqual(0, reply.AxisNumber);
			Assert.AreEqual(false, reply.Rejected);
			Assert.AreEqual(false, reply.Busy);
			Assert.AreEqual("--", reply.WarningFlags);
			Assert.AreEqual("0", reply.ResponseData);
		}


		[Test]
		public void TestReplyParsingWithChecksum()
		{
			var reply = new AsciiReply("@01 0 OK IDLE -- 0:8D");
			Assert.AreEqual(AsciiReply.ReplyType.Response, reply.MessageType);
			Assert.AreEqual(1, reply.DeviceAddress);
			Assert.AreEqual(0, reply.AxisNumber);
			Assert.AreEqual(false, reply.Rejected);
			Assert.AreEqual(false, reply.Busy);
			Assert.AreEqual("--", reply.WarningFlags);
			Assert.AreEqual("0", reply.ResponseData);
		}


		[Test]
		public void TestReplyParsingWithBadChecksum()
		{
			for (int i = 0; i < 255; ++i)
			{
				if (i != 0x8C) // The correct checksum.
				{
					var message = string.Format("@01 0 OK IDLE --0:{0:X2}", i);
					Assert.Throws<FormatException>(() =>
					{
						var reply = new AsciiReply(message);
					});
				}
			}
		}


		[Test]
		public void TestCommandParsing()
		{
			var command = new AsciiCommand("1 tools findrange");
			Assert.AreEqual(1, command.DeviceAddress);
			Assert.AreEqual(0, command.AxisNumber);
			Assert.AreEqual("tools findrange", command.Data);
			Assert.AreEqual("/1 0 tools findrange\r\n", command.ToString());
		}


		[Test]
		public void TestAsciiCommandEquality()
		{
			AsciiCommand command1 = new AsciiCommand("/1 home\n");
			Command command2 = new AsciiCommand(2, 0, "home");
			Assert.AreNotEqual(command1, command2);
			command1.DeviceAddress = 2;
			Assert.AreEqual(command1, command2);
		}


		[Test]
		public void TestAsciiReplyEquality()
		{
			var reply1 = new AsciiReply()
			{
				MessageType = AsciiReply.ReplyType.Response,
				DeviceAddress = 2,
				AxisNumber = 1,
				Rejected = true,
				Busy = false,
				WarningFlags = "--",
				ResponseData = "BADCOMMAND"
			};

			var reply2 = new AsciiReply("@02 1 RJ IDLE -- BADCOMMAND\r\n");
			var reply3 = new AsciiReply('@', 2, 1, true, false, "--", "BADCOMMAND");
			Assert.AreEqual(reply1, reply2);
			Assert.AreEqual(reply2, reply3);
			Assert.AreEqual(reply1, reply3);
		}


		[Test]
		public void TestNewlineIsTrimmed()
		{
			// Even a slightly-malformed command like this ought to be fine.
			var command = new AsciiCommand(1, 2, "/home \r\n");
			Assert.AreEqual("home", command.Data);
			var reply = new AsciiReply("@02 1 RJ IDLE -- BADCOMMAND\r\n");
			Assert.AreEqual("BADCOMMAND", reply.ResponseData);
		}


		[Test]
		public void TestCommandInequality()
		{
			Command command1 = new AsciiCommand("/\n");
			Command command2 = new BinaryCommand();
			Assert.AreNotEqual(command1, command2);
		}


		[Test]
		public void TestCommandToStringMethod()
		{
			var cmd = new AsciiCommand(1, 1, "home");
			Assert.AreEqual("/1 1 home\r\n", cmd.ToString());
		}
	}
}
