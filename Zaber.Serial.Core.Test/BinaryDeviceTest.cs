﻿using System;
using NUnit.Framework;

namespace Zaber.Serial.Core.Test
{
	[TestFixture]
	[SetCulture("en-US")]
	[Description("Tests for the " + nameof(ZaberBinaryDevice) + " class.")]
	public class BinaryDeviceTest
	{
		[SetUp]
		public void Initialize()
		{
			_port = new MockPort();
			_port.Open();
			_axis = new ZaberBinaryDevice(_port, 1);
		}


		[TearDown]
		public void Cleanup()
		{
			_port.Close();
			_port.Dispose();
		}


		[Test]
		[Description("Test busy check.")]
		public void TestIsBusy()
		{
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);
			Assert.IsTrue(_axis.IsBusy());
			Assert.IsFalse(_axis.IsBusy());
		}


		[Test]
		[Description("Test that the home command waits for idle.")]
		public void TestHomeWaitsUntilIdle()
		{
			_port.Expect(1, 1, 0);
			_port.AddResponse(1, 1, 0);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);

			_axis.Home();
			_port.Verify();
		}


		[Test]
		[Description("Test that move rel command waits for idle.")]
		public void TestMultipleMoveRel()
		{
			_port.Expect(1, 21, 10000); // First move
			_port.AddResponse(1, 21, 10000);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);
			_port.Expect(1, 21, -10000); // Second move
			_port.AddResponse(1, 21, 0);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);
			_port.Expect(1, 21, 10000); // Third move
			_port.AddResponse(1, 21, 10000);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);
			_port.Expect(1, 21, -10000); // Fourth move
			_port.AddResponse(1, 21, 0);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);

			// None of these commands pre-empt each other. The device should move
			// exactly four times.
			_axis.MoveRelative(10000);
			_axis.MoveRelative(-10000);
			_axis.MoveRelative(10000);
			_axis.MoveRelative(-10000);
			_port.Verify();
		}


		[Test]
		[Description("Test that the move abs command waits for idle, with varying wait lengths.")]
		public void TestPollUntilIdle()
		{
			// Try a few different time-to-idle loop lengths.
			for (int i = 1; i < 51; i += 10)
			{
				_port.Expect(1, 20, 1000);
				_port.AddResponse(1, 20, 1000);

				for (int j = 0; j <= i; ++j)
				{
					_port.Expect(1, 54, 0);
					_port.AddResponse(1, 54, 1);
				}

				_port.Expect(1, 54, 0);
				_port.AddResponse(1, 54, 0);

				_axis.MoveAbsolute(1000);
				_port.Verify();
			}
		}


		[Test]
		[Description("Test that the stop command waits for idle.")]
		public void TestStopWaitsUntilIdle()
		{
			_port.Expect(1, 23, 0);
			_port.AddResponse(1, 23, 12345);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 1);
			_port.Expect(1, 54, 0);
			_port.AddResponse(1, 54, 0);

			_axis.Stop();
			_port.Verify();
		}


		[Test]
		[Description("Test that the move vel command does not wait for idle.")]
		public void TestMoveVelDoesntWaitUntilIdle()
		{
			_port.Expect(1, 22, 1000);
			_port.AddResponse(1, 22, 1000);

			_axis.MoveVelocity(1000);
			_port.Verify();
		}


		[Test]
		[Description("Test getting the device position")]
		public void TestGetCurrentPosition()
		{
			_port.Expect(1, 60, 0);
			_port.AddResponse(1, 60, 12345);

			Assert.AreEqual(12345m, _axis.GetCurrentPosition());
			_port.Verify();

			_port.Expect(1, 60, 0);
			_port.AddResponse(1, 60, 54321);

			Assert.AreEqual(54321m, _axis.GetCurrentPosition());
			_port.Verify();
		}


		[Test]
		[Description("Test getting the device position, legacy edition")]
		public void TestGetPosition()
		{
			_port.Expect(1, 60, 0);
			_port.AddResponse(1, 60, 12345);

			Assert.AreEqual(12345, _axis.GetPosition());
			_port.Verify();

			_port.Expect(1, 60, 0);
			_port.AddResponse(1, 60, 54321);

			Assert.AreEqual(54321, _axis.GetPosition());
			_port.Verify();
		}


		[Test]
		[Description("Test that a missing reply propagates to user code.")]
		public void TestNoReply()
		{
			_port.Expect(1, 60, 0);

			Assert.Throws<TimeoutException>(() =>
			{
				_axis.GetCurrentPosition();
			});
		}


		[Test]
		[Description("Test that a rejected reply propagates an error to user code.")]
		public void TestRejectedReply()
		{
			_port.Expect(1, 60, 0);
			_port.AddResponse(1, 255, 64);

			Assert.Throws<UnexpectedReplyReceivedException>(() =>
			{
				_axis.GetCurrentPosition();
			});
		}


		private MockPort _port;
		private ZaberBinaryDevice _axis;
	}
}
