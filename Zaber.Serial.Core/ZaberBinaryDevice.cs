﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// Represents an axis of a device running in the binary protocol.
	/// </summary>
	/// <remarks>
	/// The binary protocol makes no distinction between devices and axes:
	/// every axis is a device in the binary protocol. Therefore this class
	/// is named ZaberBinaryDevice rather than ZaberBinaryAxis to eliminate
	/// confusion about this fact, despite implementing the IZaberAxis 
	/// interface.
	/// </remarks>
	public class ZaberBinaryDevice : IZaberAxis
	{
		/// <summary>
		/// The parent port of the axis.
		/// </summary>
		public IZaberPort Port { get; private set; }


		/// <summary>
		/// The device number, 0-255.
		/// </summary>
		public byte Number
		{
			get;
			private set;
		}


		/// <summary>
		/// Create a new instance of the ZaberBinaryDevice class to model a
		/// %Zaber device connected to the port specified by 
		/// <paramref name="port"/> using the binary protocol.
		/// </summary>
		/// <param name="port">The port to which this device is connected.</param>
		/// <param name="number">The number of this device.</param>
		public ZaberBinaryDevice(IZaberPort port, byte number)
		{
			if (null == port)
			{
				throw new ArgumentNullException("port cannot be null.");
			}

			Port = port;
			Number = number;
		}


		/// <summary>
		/// Homes the device.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void Home()
		{
			var homeCommand = new BinaryCommand(Number, 1, 0);
			Port.Write(homeCommand);
			Receive(1);
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device to the specified position.
		/// </summary>
		/// <param name="position">The position in microsteps.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveAbsolute(decimal position)
		{
			var moveAbsCommand = new BinaryCommand(Number, 20, (int)position);
			Port.Write(moveAbsCommand);
			Receive(20);
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device by a certain distance.
		/// </summary>
		/// <param name="distance">The distance in microsteps.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveRelative(decimal distance)
		{
			var moveRelCommand = new BinaryCommand(Number, 21, (int)distance);
			Port.Write(moveRelCommand);
			Receive(21);
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device at a certain speed.
		/// </summary>
		/// <remarks>
		/// This command differs from the other two move commands in that it
		/// returns immediately instead of waiting for the device to finish
		/// its movement and become idle. For this reason, the Stop() method
		/// is also provided.
		/// </remarks>
		/// <param name="speed">The speed to move at.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveVelocity(decimal speed)
		{
			var moveVelCommand = new BinaryCommand(Number, 22, (int)speed);
			Port.Write(moveVelCommand);
			Receive(22);
			// Do *not* poll until idle: allow MoveVelocity to be interrupted.
		}


		/// <summary>
		/// Stops the device.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void Stop()
		{
			var stopCommand = new BinaryCommand(Number, 23, 0);
			Port.Write(stopCommand);
			Receive(23);
			PollUntilIdle();
		}


		/// <summary>
		/// Polls the device to determine whether it is currently moving.
		/// </summary>
		/// <returns>true if the device is busy; false if it is idle.</returns>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public bool IsBusy()
		{
			var getStatusCommand = new BinaryCommand(Number, 54, 0);
			Port.Write(getStatusCommand);
			var statusReply = Receive(54);
			// 0 is the status code for "IDLE"
			return (0 != statusReply.Data);
		}


		/// <summary>
		/// Gets the position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		/// <remarks>This method is deprecated because 32-bit integers cannot
		/// represent the full range of positions on newer Zaber devices.
		/// Use <see cref="GetCurrentPosition()"/> instead.</remarks>
		[Obsolete("Use GetCurrentPosition() instead.")]
		public int GetPosition()
		{
			var getPosCommand = new BinaryCommand(Number, 60, 0);
			Port.Write(getPosCommand);
			var getPosReply = Receive(60);
			return getPosReply.Data;
		}


		/// <summary>
		/// Gets the current position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		public decimal GetCurrentPosition()
		{
			var getPosCommand = new BinaryCommand(Number, 60, 0);
			Port.Write(getPosCommand);
			var getPosReply = Receive(60);
			return getPosReply.Data;
		}


		/// <summary>
		/// Blocks while Polling the device at a regular interval until it
		/// reports itself as idle.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void PollUntilIdle()
		{
			while (IsBusy())
			{
			}
		}

		#region private helper functions

		private BinaryReply Receive(int commandNumber)
		{
			var reply = Port.Read() as BinaryReply;
			if (null == reply)
			{
				throw new UnexpectedReplyReceivedException(
					"Could not parse received data as a binary reply.");
			}

			// Ignore "Limit Active" and other move tracking
			if ((reply.CommandNumber <= 11) && (reply.CommandNumber >= 8))
			{
				// Try again recursively to get the next reply on the line
				reply = Receive(commandNumber);
			}

			if (255 == reply.CommandNumber)
			{
				throw new UnexpectedReplyReceivedException(
					string.Format("An error has occurred. Device {0} "
					+ "reported an error with code {1}. See the Binary "
					+ "Protocol Manual for more details.",
					reply.DeviceAddress, reply.Data));
			}

			if (reply.DeviceAddress != Number)
			{
				throw new UnexpectedReplyReceivedException(
					string.Format("An unexpected reply was read "
					+ "from device {0}.", reply.DeviceAddress));
			}

			if (reply.CommandNumber != commandNumber)
			{
				throw new UnexpectedReplyReceivedException(
					string.Format("A reply to a different command was read. "
					+ "Command number: {0}. Expected command number: {1}.",
					reply.CommandNumber, commandNumber));
			}

			return reply;
		}

		#endregion
	}
}
