﻿using System;
using System.Text;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class to parse and represent replies from %Zaber devices using the
	/// ASCII protocol.
	/// </summary>
	public class AsciiReply : Reply
	{
		/// <summary>
		/// Represents the three different possible types of reply.
		/// </summary>
		public enum ReplyType : ushort
		{
			/// <summary>
			/// Sent as soon as a device has received a command and determined
			/// if it should respond.
			/// </summary>
			Response = '@',

			/// <summary>
			/// Extra info from the device for testing/debugging/programming
			/// purposes.
			/// </summary>
			/// <remarks>
			/// Info is designed to be read by the user and ignored by
			/// software. One or more info messages can follow a reply or
			/// alert message. The common occurrence of info messages is in
			/// reply to the "/help" command.
			/// </remarks>
			Info = '#',

			/// <summary>
			/// Sent when a motion command has completed if a device has the
			/// "comm.alert" setting set to 1.
			/// </summary>
			Alert = '!'
		}


		/// <summary>
		/// The message type of the reply. Either '@', '#', or '!', for
		/// "response", "info", or "alert", respectively.
		/// </summary>
		public ReplyType MessageType;


		/// <summary>
		/// The axis from which the reply came, or 0 if the reply came from
		/// a whole device.
		/// </summary>
		public int AxisNumber;


		/// <summary>
		/// Whether the device rejected the last command. 
		/// </summary>
		public bool Rejected;


		/// <summary>
		/// Whether the device is moving or not.
		/// </summary>
		/// <remarks>
		/// A device will always be busy immediately after being given a
		/// "move" command. This attribute is especially useful for polling a
		/// device to see whether it is ready to be given another command.
		/// </remarks>
		public bool Busy;


		/// <summary>
		/// A two-letter code if something is wrong, or "--" if everything is
		/// OK.
		/// </summary>
		public string WarningFlags;


		/// <summary>
		/// The "meat" of a response. Can contain many different things: see
		/// the command reference for your chosen test to see what to expect.
		/// </summary>
		public string ResponseData;


		/// <summary>
		/// Create a blank AsciiReply.
		/// </summary>
		/// <remarks>
		/// Most of the time, you will want to use the one-argument AsciiReply
		/// constructor, which will parse a reply string and turn it into a
		/// populated AsciiReply object.
		/// </remarks>
		public AsciiReply()
		{
		}


		/// <summary>
		/// Create a new AsciiReply parsed from a string.
		/// </summary>
		/// <remarks>
		/// It is rare that you will need to construct an AsciiReply yourself:
		/// ZaberPort packages up received strings as AsciiReplies
		/// automatically. This constructor parses a reply and builds a
		/// matching AsciiReply object.
		/// </remarks>
		/// <param name="reply">The string to be parsed.</param>
		/// <exception cref="ArgumentNullException"><paramref name="reply"/>
		/// is null.</exception>
		/// <exception cref="FormatException">The <paramref name="reply"/>
		/// could not be parsed or the message checksum, if present, is wrong.</exception>
		public AsciiReply(string reply)
		{
			if (null == reply)
			{
				throw new ArgumentNullException("reply must not be null.");
			}

			// Pop off the message type identifier.
			MessageType = (ReplyType)reply[0];
			reply = reply.Substring(1);

			// Handle checksum if present.
			string[] tokens = reply.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
			reply = tokens[0];
			if (tokens.Length > 1)
			{
				byte checksum = 0;
				if (!byte.TryParse(tokens[1].Trim(), 
								   System.Globalization.NumberStyles.HexNumber, 
								   System.Globalization.CultureInfo.InvariantCulture, 
								   out checksum))
				{
					throw new FormatException("Reply checksum could not be parsed.");
				}

				foreach (var c in Encoding.ASCII.GetBytes(reply))
				{
					checksum = (byte)((checksum + c) & 0xFF);
				}

				if (0 != checksum)
				{
					throw new FormatException("Reply checksum is incorrect.");
				}
			}

			// "Response" type reply.
			if (ReplyType.Response == MessageType)
			{
				tokens = reply.Split((char[])null, 6, StringSplitOptions.RemoveEmptyEntries);

				if (tokens.Length < 6)
				{
					throw new FormatException("Malformed reply string: " + reply);
				}

				DeviceAddress = byte.Parse(tokens[0]);
				AxisNumber = int.Parse(tokens[1]);
				Rejected = tokens[2].Equals("RJ");
				Busy = tokens[3].Equals("BUSY");
				WarningFlags = tokens[4];
				ResponseData = tokens[5];
			}
			else if (ReplyType.Alert == MessageType)
			{
				tokens = reply.Split((char[])null, 4, StringSplitOptions.RemoveEmptyEntries);

				if (tokens.Length < 4)
				{
					throw new FormatException("Malformed reply string: " + reply);
				}

				DeviceAddress = byte.Parse(tokens[0]);
				AxisNumber = int.Parse(tokens[1]);
				Busy = tokens[2].Equals("BUSY");
				WarningFlags = tokens[3];
				ResponseData = "";
			}

			else if (ReplyType.Info == MessageType)
			{
				tokens = reply.Split((char[])null, 3, StringSplitOptions.RemoveEmptyEntries);

				if (tokens.Length < 3)
				{
					throw new FormatException("Malformed reply string: " + reply);
				}

				DeviceAddress = byte.Parse(tokens[0]);
				AxisNumber = int.Parse(tokens[1]);
				ResponseData = tokens[2];
			}

			// Reply type unrecognized.
			else
			{
				throw new FormatException("Unrecognized message type: " + MessageType);
			}

			ResponseData = ResponseData.TrimEnd(new char[] { ' ', '\r', '\n' });
		}


		/// <summary>
		/// A constructor for making AsciiReplies by explicitly settings each
		/// attribute.
		/// </summary>
		/// <param name="messageType">The type of the reply.</param>
		/// <param name="deviceAddress">The address of the device which sent
		/// the reply.</param>
		/// <param name="axisNumber">The number of the axis which sent the 
		/// reply.</param>
		/// <param name="rejected">Whether a command was rejected by the
		/// device.</param>
		/// <param name="busy">Whether the device is busy.</param>
		/// <param name="warningFlags">A flag warning that something may be
		/// wrong with the device or its operation.</param>
		/// <param name="responseData">The data associated with a reply.
		/// </param>
		public AsciiReply(char messageType, byte deviceAddress,
			int axisNumber, bool rejected, bool busy,
			string warningFlags, string responseData)
		{
			MessageType = (ReplyType)messageType;
			DeviceAddress = deviceAddress;
			AxisNumber = axisNumber;
			Rejected = rejected;
			Busy = busy;
			WarningFlags = warningFlags;
			ResponseData = responseData.TrimEnd(new char[] { ' ', '\r', '\n' });
		}


		/// <summary>
		/// Checks to see if this reply is equal to another Reply.
		/// </summary>
		/// <param name="reply">The reply to compare this one to.</param>
		/// <returns>True if the replies are equal, false if inequal.</returns>
		public override bool Equals(Reply reply)
		{
			var asciiReply = reply as AsciiReply;
			if (null == asciiReply)
			{
				return false;
			}

			return Equals(asciiReply);
		}


		/// <summary>
		/// Reports whether an AsciiReply is equal to another AsciiReply.
		/// </summary>
		/// <remarks>Equality is defined as having all of the same attribute
		/// values. If any of the MessageType, DeviceAddress, AxisNumber, etc.
		/// are inequal, this method returns false.</remarks>
		/// <param name="reply">The AsciiReply to which to compare this
		/// AsciiReply.</param>
		/// <returns>true if equal, false if inequal.</returns>
		public bool Equals(AsciiReply reply)
		{
			if (ReferenceEquals(this, reply))
			{
				return true;
			}

			if (MessageType != reply.MessageType
			 || DeviceAddress != reply.DeviceAddress
			 || AxisNumber != reply.AxisNumber
			 || !Rejected.Equals(reply.Rejected)
			 || !Busy.Equals(reply.Busy)
			 || !WarningFlags.Equals(reply.WarningFlags)
			 || !ResponseData.Equals(reply.ResponseData))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// Returns a hash code based off the reply's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public override int GetHashCode()
		{
			int result = 17;

			result = 31 * result + (ushort)MessageType;
			result = 31 * result + DeviceAddress;
			result = 31 * result + AxisNumber;

			if (Rejected)
			{
				result = 31 * result + Rejected.GetHashCode();
			}

			if (Busy)
			{
				result = 31 * result + Busy.GetHashCode();
			}

			if (null != WarningFlags)
			{
				result = 31 * result + WarningFlags.GetHashCode();
			}

			if (null != ResponseData)
			{
				result = 31 * result + ResponseData.GetHashCode();
			}

			return result;
		}


		/// <summary>
		/// Returns a human-readable representation of this reply.
		/// </summary>
		/// <remarks>
		/// This method will return a string which is either identical or very
		/// similar to the original reply string. 
		/// </remarks>
		/// <returns>A string representing the reply.</returns>
		public override string ToString()
		{
			return string.Format("{0}{1:D2} {2} {3} {4} {5} {6}\r\n",
				(char)MessageType,
				DeviceAddress,
				AxisNumber,
				Rejected ? "RJ" : "OK",
				Busy ? "BUSY" : "IDLE",
				WarningFlags,
				ResponseData);
		}
	}
}
