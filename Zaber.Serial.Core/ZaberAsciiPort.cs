﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// Represents a port connected to devices using the ASCII protocol.
	/// </summary>
	public class ZaberAsciiPort : IZaberPort, IDisposable
	{
		/// <summary>
		/// Indicates that no time-out should occur.
		/// </summary>
		public const int InfiniteTimeout = SerialPort.InfiniteTimeout;


		/// <summary>
		/// Initializes a new instance of the ZaberAsciiPort class using the
		/// specified port name.
		/// </summary>
		/// <param name="portName">The port to use (for example, COM1).</param>
		/// <exception cref="System.IO.IOException">The specified port could not be
		/// found or opened.</exception>
		public ZaberAsciiPort(string portName)
		: this(portName, 115200)
		{
		}


		/// <summary>
		/// Initializes a new instance of the ZaberAsciiPort class using the
		/// specified port name and baud rate.
		/// </summary>
		/// <param name="portName">The port to use (for example, COM1).</param>
		/// <param name="baudRate">The baud rate.</param>
		/// <exception cref="ArgumentException">portName is either null, or 
		/// does not begin with "COM".</exception>
		public ZaberAsciiPort(string portName, int baudRate)
		{
			if (null == portName)
			{
				throw new ArgumentException("portName cannot be null, "
					+ "and must begin with 'COM'.");
			}

			if (baudRate != 115200 // short-circuit to the most common
				&& baudRate != 9600
				&& baudRate != 19200
				&& baudRate != 38400
				&& baudRate != 57600)
			{
				throw new ArgumentException("Valid baud rates are 9600, "
					+ "19200, 38400, 57600, and 115200.");
			}

			_port = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
		}


		/// <summary>
		/// Gets the serial baud rate.
		/// </summary>
		public int BaudRate
		{
			get { return _port.BaudRate; }
		}

		#region -- IZaberPort implementation --

		/// <summary>
		/// The name of the port.
		/// </summary>
		public string PortName
		{
			get { return _port.PortName; }
		}


		/// <summary>
		/// Whether the port is open.
		/// </summary>
		public bool IsOpen
		{
			get { return _port.IsOpen; }
		}


		/// <summary>
		/// The number of milliseconds before a time-out occurrs when a read
		/// operation does not finish.
		/// </summary>
		/// <remarks>
		/// This property allows you to set the read time-out value.
		/// The time-out can be set to any value greater than zero, or set to
		/// -1, in which case no time-out occurrs. -1 is the default.
		/// </remarks>
		public int ReadTimeout
		{
			get { return _port.ReadTimeout; }
			set
			{
				if (-1 == value)
				{
					// SerialPort.InfiniteTimeout *is* -1, but that's an 
					// implementation detail we can't rely on.
					_port.ReadTimeout = SerialPort.InfiniteTimeout;
				}
				else
				{
					_port.ReadTimeout = value;
				}
			}
		}


		/// <summary>
		/// Opens a new serial port connection.
		/// </summary>
		/// <remarks>
		/// This method closes and re-opens the port if the port is already
		/// open.
		/// </remarks>
		/// <exception cref="UnauthorizedAccessException">Access is denied to
		/// the port, or the current process or another process on the system
		/// already has the specified COM port open.</exception>
		/// <exception cref="System.IO.IOException">An attempt to open the port failed.
		/// Often this is caused by a port name being incorrect, or a port
		/// being disconnected.</exception>
		public void Open()
		{
			if (IsOpen)
			{
				Close();
			}

			_port.Open();
		}


		/// <summary>
		/// Closes the underlying SerialPort object's connection.
		/// </summary>
		/// <remarks>
		/// This method will only try to close the port if it is open.
		/// Attempting to close an already-closed port will do nothing.
		/// </remarks>
		public void Close()
		{
			if (IsOpen)
			{
				_port.Close();
			}
		}


		/// <summary>
		/// Sends a command to the port's devices.
		/// </summary>
		/// <param name="command">The command to be sent.</param>
		/// <exception cref="ArgumentNullException">The 
		/// <paramref name="command"/> passed is null.</exception>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Write(Command command)
		{
			var cmd = command as AsciiCommand;
			if (null == cmd)
			{
				throw new ArgumentNullException("command cannot be null and must be an AsciiCommand.");
			}

			_port.Write(cmd.ToString());
		}


		/// <summary>
		/// Reads a reply from a device.
		/// </summary>
		/// <returns>The reply read.</returns>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		/// <exception cref="TimeoutException">No bytes were available to
		/// read.</exception>
		Reply IZaberPort.Read()
		{
			var str = _port.ReadLine();
			return new AsciiReply(str);
		}


		/// <summary>
		/// Purges all input waiting on the input buffer, and attemps to drain
		/// all incoming data before returning.
		/// </summary>
		/// <remarks>
		/// This method will block until there is a 100ms period of silence in
		/// serial communication, guaranteeing that the next time Read() is
		/// called, it will receive a full message sent by the device some
		/// time after the call to Drain().
		/// </remarks>
		/// <remarks>
		/// This method is intended to be used when many commands have been
		/// sent without receiving any replies. If the user must suddenly read
		/// a reply to a certain command and has not been receiving replies
		/// otherwise, this method will allow them to immediately read the
		/// next incoming reply.
		/// </remarks>
		/// <remarks>
		/// Do not use this method in any setting where a serial line may
		/// never be quiet for over 100ms, as it may block forever. Some
		/// examples of such a setup are multiple devices chained together
		/// being polled constantly for position, or at least one device
		/// connected with move tracking enabled.
		/// </remarks>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Drain()
		{
			_port.DiscardInBuffer();

			int oldTimeout = ReadTimeout;
			ReadTimeout = 100;

			// Synchronously read until there's nothing for 100ms.
			while (true)
			{
				try
				{
					_port.ReadByte();
				}
				catch (TimeoutException)
				{
					break; // drained!
				}
			}

			ReadTimeout = oldTimeout;
		}


		/// <summary>
		/// Queries the port to find all connected axes.
		/// </summary>
		/// <remarks>
		/// This method will only find axes which respond at the specified
		/// baud rate and protocol.
		/// </remarks>
		/// <returns>A list of connected axes.</returns>
		public List<IZaberAxis> FindAxes()
		{
			var axes = new List<IZaberAxis>();
			Write("/get system.axiscount\r\n");

			int oldTimeout = ReadTimeout;
			ReadTimeout = 50;

			while (true)
			{
				try
				{
					var reply = Read();
					int nAxes = int.Parse(reply.ResponseData);
					for (int i = 1; i <= nAxes; i++)
					{
						// The 3-argument ZaberAsciiAxis constructor will create
						// its parent ZaberAsciiDevice if necessary, and the two 
						// constructors will do all the list-adding for us.
						axes.Add(new ZaberAsciiAxis(this, reply.DeviceAddress, i));
					}
				}
				catch (TimeoutException)
				{
					break; // No more devices responded.
				}
			}

			ReadTimeout = oldTimeout;

			return axes;
		}

		#endregion

		/// <summary>
		/// Writes a command string to the port.
		/// </summary>
		/// <remarks>
		/// This method checks whether the <paramref name="command"/> passed
		/// starts with '/' and ends with '\\n', and adds them if they are
		/// absent.
		/// </remarks>
		/// <param name="command">A string to be sent.</param>
		/// <exception cref="ArgumentNullException">The 
		/// <paramref name="command"/> passed is null.</exception>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Write(string command)
		{
			if (null == command)
			{
				throw new ArgumentNullException("command cannot be null.");
			}

			if (!command.StartsWith("/"))
			{
				command = '/' + command;
			}

			if (!command.EndsWith("\n"))
			{
				command = command + '\n';
			}

			_port.Write(command);
		}


		/// <summary>
		/// Reads a reply from a device.
		/// </summary>
		/// <returns>The reply read.</returns>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		/// <exception cref="TimeoutException">No bytes were available to
		/// read.</exception>
		public AsciiReply Read()
		{
			var str = _port.ReadLine();
			return new AsciiReply(str);
		}


		/// <summary>
		/// Queries the port to find all connected devices.
		/// </summary>
		/// <remarks>
		/// This method will only find devices which respond at the specified
		/// baud rate and protocol.
		/// </remarks>
		/// <remarks>
		/// In general, it is recommended to use FindAxes() to get all of the
		/// axes connected to a port. There are plenty of good reasons for
		/// getting a list of devices instead, but we encourage sending 
		/// movement commands to individual axes, even if the device has only
		/// one axis. In any case, communication with devices should be done
		/// in terms of the device's axes, which can be done from
		/// FindDevices() like so:
		/// <code>
		/// // Home all connected devices' first axes.
		/// foreach (ZaberAsciiDevice device in port.FindDevices())
		/// {
		///     device.GetAxis(1).Home();
		/// }
		/// </code>
		/// </remarks>
		/// <returns>A list of connected devices.</returns>
		public List<ZaberAsciiDevice> FindDevices()
		{
			var devices = new List<ZaberAsciiDevice>();
			Write("/\r\n");

			int oldTimeout = ReadTimeout;
			ReadTimeout = 50;

			while (true)
			{
				try
				{
					var reply = Read();
					devices.Add(new ZaberAsciiDevice(this, reply.DeviceAddress));
				}
				catch (TimeoutException)
				{
					break; // No more devices responded.
				}
			}

			ReadTimeout = oldTimeout;

			return devices;
		}


		/// <summary>
		/// Releases the resources used by the underlying SerialPort object.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}


		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				_port.Dispose();
			}
		}


		private SerialPort _port;
	}
}
