﻿using System;
using System.Runtime.Serialization;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// Thrown when an axis receives a reply from a device or axis
	/// which is not itself, or when the reply is of an unexpected type.
	/// </summary>
	/// <remarks>
	/// This exception is usually thrown when the input buffer has become
	/// shifted or misaligned. Generally this is caused by a Write() not
	/// being followed by a Read() somewhere when dealing with a port
	/// object directly.
	/// </remarks>
	[Serializable]
	public class UnexpectedReplyReceivedException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the 
		/// UnexpectedReplyReceivedException class.
		/// </summary>
		public UnexpectedReplyReceivedException()
		: base()
		{
		}


		/// <summary>
		/// Initializes a new instance of the
		/// UnexpectedReplyReceivedException class with a specified error
		/// message.
		/// </summary>
		/// <param name="message">The message that describes the error.
		/// </param>
		public UnexpectedReplyReceivedException(string message)
		: base(message)
		{
		}


		/// <summary>
		/// Initializes a new instance of the 
		/// UnexpectedReplyReceivedException class with serialized data.
		/// </summary>
		/// <param name="info">The SerializationInfo that holds the
		/// serialized object data about the exception being thrown.
		/// </param>
		/// <param name="context">The StreamingContext that contains
		/// contextual information about the source or destination.
		/// </param>
		protected UnexpectedReplyReceivedException(SerializationInfo info,
			StreamingContext context)
		: base(info, context)
		{
		}
	}

}
