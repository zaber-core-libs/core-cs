﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// An interface to represent an individual %Zaber device's axis.
	/// </summary>
	/// <remarks>
	/// Classes implementing this interface are intended only for basic,
	/// one-axis-at-a-time use cases. If you need to synchronize devices,
	/// move more than one axis at a time, or do any other sort of
	/// asynchronous I/O, use a class which implements IZaberPort.
	/// 
	/// Classes implementing IZaberAxis make a few assumptions, the most 
	/// fundamental assumption being that all interaction with devices is done
	/// exclusively using the commands in this interface. From this assumption,
	/// it makes the second assumption that all input read immediately after a
	/// write is the anticipated response. Therefore, if you intend to mix and
	/// match calls to things like ZaberAsciiAxis.Home() with calls to
	/// ZaberAsciiPort.Write(), it is essential to follow every call to
	/// Write() with a Read(), or the input buffer will become misaligned. 
	/// </remarks>
	public interface IZaberAxis
	{
		/// <summary>
		/// The parent port of the axis.
		/// </summary>
		IZaberPort Port
		{
			get;
		}


		/// <summary>
		/// Homes the device.
		/// </summary>
		void Home();


		/// <summary>
		/// Moves the device to the specified position.
		/// </summary>
		/// <param name="position">The position in microsteps.</param>
		void MoveAbsolute(decimal position);


		/// <summary>
		/// Moves the device by a certain distance.
		/// </summary>
		/// <param name="distance">The distance in microsteps.</param>
		void MoveRelative(decimal distance);


		/// <summary>
		/// Moves the device at a certain speed.
		/// </summary>
		/// <remarks>
		/// This command differs from the other two move commands in that it
		/// returns immediately instead of waiting for the device to finish
		/// its movement and become idle. For this reason, the Stop() method
		/// is also provided.
		/// </remarks>
		/// <param name="speed">The speed to move at.</param>
		void MoveVelocity(decimal speed);


		/// <summary>
		/// Stops the device.
		/// </summary>
		void Stop();


		/// <summary>
		/// Polls the device to determine whether it is currently moving.
		/// </summary>
		/// <returns>true if the device is busy; false if it is idle.</returns>
		bool IsBusy();


		/// <summary>
		/// Gets the position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		/// <remarks>This method is deprecated because 32-bit integers cannot
		/// represent the full range of positions on newer Zaber devices.
		/// Use <see cref="GetCurrentPosition()"/> instead.</remarks>
		[Obsolete("Use GetCurrentPosition() instead.")]
		int GetPosition();


		/// <summary>
		/// Gets the current position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		decimal GetCurrentPosition();


		/// <summary>
		/// Blocks while Polling the device at a regular interval until it
		/// reports itself as idle.
		/// </summary>
		void PollUntilIdle();
	}
}
