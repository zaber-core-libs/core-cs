﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// An abstract class for replies of all forms to inherit from.
	/// </summary>
	public abstract class Reply : IEquatable<Reply>
	{
		/// <summary>
		/// The address of the device from which the reply came.
		/// </summary>
		public byte DeviceAddress;


		/// <summary>
		/// Creates a new instance of the Reply class.
		/// </summary>
		protected Reply()
		{
			// pass
		}


		/// <summary>
		/// Returns a hash code based off the reply's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public abstract override int GetHashCode();


		/// <summary>
		/// Returns a human-readable representation of this reply.
		/// </summary>
		/// <returns>A string representing the reply.</returns>
		public abstract override string ToString();


		/// <summary>
		/// Tests the equality of any object against this reply.
		/// </summary>
		/// <param name="obj">The object to be compared.</param>
		/// <returns>True if the object and this reply are equal,
		/// false if they are inequal.</returns>
		public override bool Equals(object obj)
		{
			Reply reply = obj as Reply;
			// either the above boxing didn't work, or obj is truly null
			if (null == obj)
			{
				return false;
			}

			return Equals(reply);
		}


		/// <summary>
		/// Checks to see if this reply is equal to another Reply.
		/// </summary>
		/// <param name="reply">The reply to compare this one to.</param>
		/// <returns>True if the replies are equal, false if inequal.</returns>
		public abstract bool Equals(Reply reply);
	}
}
