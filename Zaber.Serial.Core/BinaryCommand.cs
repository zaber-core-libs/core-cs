﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class for representing and encoding 6-byte binary commands.
	/// </summary>
	public class BinaryCommand : Command
	{
		/// <summary>
		/// The number of the command to send.
		/// </summary>
		public byte CommandNumber = 54;


		/// <summary>
		/// The data value to send in the command.
		/// </summary>
		public int Data = 0;


		/// <summary>
		/// Create a blank BinaryCommand.
		/// </summary>
		/// <remarks>
		/// Though this constructor should be thought of as creating a blank
		/// command, integers must be set to something by default, which is
		/// usually 0. This is dangerous, as command number 0 is the "reset"
		/// command. Therefore, the default command number is instead 54, the
		/// "get status" command.
		/// </remarks>
		public BinaryCommand()
		{
		}


		/// <summary>
		/// Create a BinaryCommand from 6 bytes in the proper form to be sent
		/// to a device using the binary protocol.
		/// </summary>
		/// <param name="command">A byte array, the first six bytes of which
		/// will be converted into a BinaryCommand object.</param>
		public BinaryCommand(byte[] command)
		{
			DeviceAddress = command[0];
			CommandNumber = command[1];
			Data = command[5] << 24 | command[4] << 16 | command[3] << 8 | command[2];
		}


		/// <summary>
		/// The recommended constructor, creating a BinaryCommand from user-
		/// specified parameters.
		/// </summary>
		/// <param name="deviceAddress">The number of the target device.
		/// </param>
		/// <param name="commandNumber">The number of the command to send.
		/// </param>
		/// <param name="data">The data to send with the command.</param>
		public BinaryCommand(byte deviceAddress, byte commandNumber, int data)
		{
			DeviceAddress = deviceAddress;
			CommandNumber = commandNumber;
			Data = data;
		}


		/// <summary>
		/// Checks to see if this command is equal to another Command.
		/// </summary>
		/// <param name="command">The command to compare this one to.</param>
		/// <returns>True if the commands are equal, false if inequal.</returns>
		public override bool Equals(Command command)
		{
			var binaryCommand = command as BinaryCommand;

			// If the types match, cast appropriately and carry on.
			if (null == binaryCommand)
			{
				return false;
			}

			return Equals(binaryCommand);
		}


		/// <summary>
		/// Reports whether a BinaryCommand is equal to this one.
		/// </summary>
		/// <remarks>Equality is defined as having all of the same attribute
		/// values. If any of the objects' attributes are inequal, this method
		/// returns false.</remarks>
		/// <param name="command">The BinaryCommand to which to compare this
		/// BinaryCommand.</param>
		/// <returns>true if equal, false if inequal.</returns>
		public bool Equals(BinaryCommand command)
		{
			// An instance is definitely equal to itself.
			if (ReferenceEquals(this, command))
			{
				return true;
			}

			if (DeviceAddress != command.DeviceAddress
			 || CommandNumber != command.CommandNumber
			 || Data != command.Data)
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// Returns a hash code based off the command's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public override int GetHashCode()
		{
			int result = 17;

			result = 31 * result + DeviceAddress;
			result = 31 * result + CommandNumber;
			result = 31 * result + Data;

			return result;
		}


		/// <summary>
		/// Returns a human-readable representation of this command.
		/// </summary>
		/// <returns>A string representing the command.</returns>
		public override string ToString()
		{
			return string.Format("[{0}, {1}, {2}]", DeviceAddress, CommandNumber, Data);
		}


		/// <summary>
		/// Encodes the command represented by this object into an array of
		/// bytes in the form specified by the %Zaber Binary Protocol Manual.
		/// </summary>
		/// <returns>A 6-byte array to be transmitted to a device.</returns>
		public byte[] ToByteArray()
		{
			byte[] array = new byte[6];

			array[0] = DeviceAddress;
			array[1] = CommandNumber;

			/* C# Language Specification v5.0, section 7.9 (Shift operators):
			 * "When the left operand of the >> operator is of a signed
			 * integral type, the operator performs an arithmetic shift right
			 * wherein the value of the most significant bit (the sign bit) of
			 * the operand is propagated to the high-order empty bit 
			 * positions. When the left operand of the >> operator is of an 
			 * unsigned integral type, the operator performs a logical shift
			 * right wherein high-order empty bit positions are always set to
			 * zero. To perform the opposite operation of that inferred from
			 * the operand type, explicit casts can be used. For example, if x
			 * is a variable of type int, the operation 
			 * unchecked((int)((uint)x >> y)) performs a logical shift right 
			 * of x." */
			unchecked
			{
				uint dataCopy = (uint)Data;
				for (int i = 2; i < 6; i++)
				{
					/* From section 6.2.1 (Explicit numeric conversions):
					 * "In an unchecked context, the conversion always
					 * succeeds, and proceeds as follows.
					 * If the source type is larger than the destination type,
					 * then the source value is truncated by discarding its 
					 * “extra” most significant bits. The result is then 
					 * treated as a value of the destination type." */
					array[i] = (byte)dataCopy;
					dataCopy >>= 8;
				}
			}

			return array;
		}
	}
}
