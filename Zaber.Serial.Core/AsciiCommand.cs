﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class to represent ASCII commands to be sent to %Zaber devices.
	/// </summary>
	public class AsciiCommand : Command
	{
		/// <summary>
		/// The number of the axis to which to send a command, or 0 to send a
		/// command to all axes.
		/// </summary>
		/// <remarks>
		/// Most commands must be sent to axis 0: only certain commands can
		/// be sent to particular axes. See the %Zaber ASCII Protocol Manual
		/// for more info.
		/// </remarks>
		public int AxisNumber = 0;


		/// <summary>
		/// The data of the command to be sent to the device.
		/// </summary>
		/// <remarks>
		/// The data string may contain one or more command keywords,
		/// as well as extra data related to that command.
		/// </remarks>
		public string Data = "";


		/// <summary>
		/// Create an empty command.
		/// </summary>
		/// <remarks>
		/// Data is set to the empty string instead of null by default, so
		/// this constructor can be used to quickly create the minimal ASCII
		/// command (<c>"/\n"</c>).
		/// </remarks>
		public AsciiCommand()
		{
		}


		/// <summary>
		/// Parses the string "command" and creates a representation of that
		/// ASCII command.
		/// </summary>
		/// <param name="command">The string to be parsed into a Command
		/// object.</param>
		public AsciiCommand(string command)
		{
			if (command.StartsWith("/"))
			{
				command = command.Substring(1);
			}

			string[] tokens = command.Split((char[])null, 3);
			int i = -1;
			try
			{
				DeviceAddress = byte.Parse(tokens[++i]); // tokens[0]
				AxisNumber = int.Parse(tokens[++i]); // tokens[1]
				Data = tokens[++i]; // tokens[2]
			}
			catch (FormatException)
			{
				// No device number, or no axis number.
				// Join together the remaining tokens as Data.
				Data = string.Join(" ", tokens, i, tokens.Length - i);
			}

			Data = Data.TrimEnd(new char[] { ' ', '\r', '\n' });
		}


		/// <summary>
		/// Create a new AsciiCommand for a specified ZaberAsciiAxis.
		/// </summary>
		/// <remarks>
		/// This constructor is an alias for <c>AsciiCommand(
		/// targetAxis.Parent.Address, targetAxis.Number, data)</c>. It is
		/// provided as a convenience.
		/// </remarks>
		/// <param name="targetAxis">The axis to which to send this command.
		/// </param>
		/// <param name="data">The data of the command to be sent, including
		/// command keywords and any command parameters.</param>
		public AsciiCommand(ZaberAsciiAxis targetAxis, string data)
		: this(targetAxis.ParentAddress, targetAxis.Number, data)
		{
		}


		/// <summary>
		/// Create a new AsciiCommand from a specified set of arguments.
		/// </summary>
		/// <param name="deviceAddress">The address of the device to which to
		/// send this command.</param>
		/// <param name="axisNumber">The number of the axis to which to send
		/// this command.</param>
		/// <param name="data">The data of the command to be sent, including
		/// command keywords and any command parameters.</param>
		public AsciiCommand(byte deviceAddress, int axisNumber, string data)
		{
			DeviceAddress = deviceAddress;
			AxisNumber = axisNumber;

			if (null != data)
			{
				if (data.StartsWith("/"))
				{
					data = data.Substring(1);
				}

				Data = data.TrimEnd(new char[] { ' ', '\r', '\n' });
			}
		}


		/// <summary>
		/// Checks to see if this command is equal to another Command.
		/// </summary>
		/// <param name="command">The command to compare this one to.</param>
		/// <returns>True if the commands are equal, false if inequal.</returns>
		public override bool Equals(Command command)
		{
			var asciiCommand = command as AsciiCommand;

			// If the types match, cast appropriately and carry on.
			if (null == asciiCommand)
			{
				return false;
			}

			return Equals(asciiCommand);
		}


		/// <summary>
		/// Checks whether this AsciiCommand is equal to another one.
		/// </summary>
		/// <remarks>
		/// Equality is based on attribute equality, including mutable
		/// attributes. There is no guarantee that if a Command equals another
		/// at a certain time, it will always equal that other Command in the
		/// future.
		/// </remarks>
		/// <param name="command">The command to which to compare this one.
		/// </param>
		/// <returns>True if the commands are equal, false if they are
		/// inequal.</returns>
		public bool Equals(AsciiCommand command)
		{
			// An instance is definitely equal to itself.
			if (ReferenceEquals(this, command))
			{
				return true;
			}

			if (DeviceAddress != command.DeviceAddress
				|| AxisNumber != command.AxisNumber
				|| !Data.Equals(command.Data))
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// Returns a hash code based off the command's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public override int GetHashCode()
		{
			int result = 17;

			result = 31 * result + DeviceAddress;
			result = 31 * result + AxisNumber;
			result = 31 * result + Data.GetHashCode();

			return result;
		}


		/// <summary>
		/// Returns a human-readable representation of this command.
		/// </summary>
		/// <remarks>
		/// This method will return the "encoded" string format of the command
		/// which should be sent to the device. This is also the format
		/// expected by the AsciiCommand constructor which takes one argument,
		/// a string.
		/// </remarks>
		/// <returns>A string representing the command.</returns>
		public override string ToString()
		{
			return string.Format("/{0} {1} {2}\r\n", DeviceAddress, AxisNumber, Data);
		}
	}
}
