﻿namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class for representing replies from devices using the binary 
	/// protocol.
	/// </summary>
	public class BinaryReply : Reply
	{
		/// <summary>
		/// The number of the command which caused the device to reply.
		/// </summary>
		public byte CommandNumber = 54;


		/// <summary>
		/// Data returned with the reply.
		/// </summary>
		public int Data = 0;


		/// <summary>
		/// Create an empty reply object.
		/// </summary>
		/// <remarks>
		/// <c>int</c>s need to be set to something by default. This is
		/// typically 0. In Zaber's binary protocol, 0 is the "reset" command.
		/// To avoid accidental resetting a device or giving the impression a
		/// device has reset, the CommandNumber is set by default instead to
		/// 54, the more-harmless "get status" command. 
		/// </remarks>
		public BinaryReply()
		{
		}


		/// <summary>
		/// Parses a byte array and creates a matching BinaryReply.
		/// </summary>
		/// <remarks>
		/// ZaberPort automatically calls this constructor to parse a reply
		/// received from the port before returning the Reply from Receive().
		/// For this reason, it is rare that a user would need to use this
		/// constructor directly, or any BinaryReply constructor for that
		/// matter.
		/// </remarks>
		/// <param name="reply">The byte array to be parsed into a BinaryReply
		/// object.</param>
		public BinaryReply(byte[] reply)
		{
			DeviceAddress = reply[0];
			CommandNumber = reply[1];
			Data = reply[5] << 24 | reply[4] << 16 | reply[3] << 8 | reply[2];
		}


		/// <summary>
		/// Create a new BinaryReply from a set of parameters.
		/// </summary>
		/// <remarks>
		/// This constructor is mostly useful for testing purposes, or to
		/// more easily compare a reply with an expected response.
		/// </remarks>
		/// <param name="deviceAddress">The number of the device this reply
		/// "came from".</param>
		/// <param name="commandNumber">The number of the command that caused
		/// the "device" to respond.</param>
		/// <param name="data">The data to be sent along with the command.</param>
		public BinaryReply(byte deviceAddress, byte commandNumber, int data)
		{
			DeviceAddress = deviceAddress;
			CommandNumber = commandNumber;
			Data = data;
		}


		/// <summary>
		/// Checks to see if this reply is equal to another Reply.
		/// </summary>
		/// <param name="reply">The reply to compare this one to.</param>
		/// <returns>True if the replies are equal, false if inequal.</returns>
		public override bool Equals(Reply reply)
		{
			var binaryReply = reply as BinaryReply;

			if (null == binaryReply)
			{
				return false;
			}

			return Equals(binaryReply);
		}


		/// <summary>
		/// Reports whether this BinaryReply is equal to another BinaryReply.
		/// </summary>
		/// <param name="reply">The reply to be compared to this one.</param>
		/// <returns>True if the replies are equal, false if inequal.</returns>
		public bool Equals(BinaryReply reply)
		{
			if (ReferenceEquals(this, reply))
			{
				return true;
			}

			if (DeviceAddress != reply.DeviceAddress
			 || CommandNumber != reply.CommandNumber
			 || Data != reply.Data)
			{
				return false;
			}

			return true;
		}


		/// <summary>
		/// Returns a hash code based off the reply's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public override int GetHashCode()
		{
			int result = 17;

			result = 31 * result + DeviceAddress;
			result = 31 * result + CommandNumber;
			result = 31 * result + Data;

			return result;
		}


		/// <summary>
		/// Returns a human-readable representation of this reply.
		/// </summary>
		/// <returns>A string representing the reply.</returns>
		public override string ToString()
		{
			return string.Format("[{0}, {1}, {2}]", DeviceAddress, CommandNumber, Data);
		}
	}
}
