﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class to model an individual axis of an ASCII device.
	/// </summary>
	/// <remarks>
	/// This class should be the de-facto way to interact with %Zaber devices
	/// using the ASCII protocol (not ZaberAsciiDevice).
	/// </remarks>
	public class ZaberAsciiAxis : IZaberAxis
	{

		/// <summary>
		/// The parent port of the axis.
		/// </summary>
		public IZaberPort Port { get; private set; }


		/// <summary>
		/// The address of the parent device.
		/// </summary>
		public byte ParentAddress;


		/// <summary>
		/// The number of this axis.
		/// </summary>
		public int Number;


		/// <summary>
		/// Creates a new instance of the ZaberAsciiAxis class.
		/// </summary>
		/// <remarks>
		/// This three-argument constructor will first check in the device
		/// list of the <paramref name="port"/> specified to see if a device
		/// with the specified address exists. If so, the constructor adds
		/// this axis to that device's list of axes. If not, it creates an
		/// appropriate device object, adds it to the port's list, and then
		/// registers itself with that device.
		/// </remarks>
		/// <remarks>
		/// No matter what, the result of this constructor will be that the
		/// <paramref name="port"/> will have a device with address 
		/// <paramref name="deviceAddress"/>, which will have an axis (this
		/// newly-created object) with number <paramref name="axisNumber"/>.
		/// </remarks>
		/// <param name="port">The port this axis is connected to.</param>
		/// <param name="deviceAddress">The address of the parent device.</param>
		/// <param name="axisNumber">The axis number.</param>
		public ZaberAsciiAxis(IZaberPort port, byte deviceAddress, int axisNumber)
		{
			if (null == port)
			{
				throw new ArgumentNullException("Port cannot be null.");
			}

			if (axisNumber < 1 || axisNumber > 9)
			{
				throw new ArgumentOutOfRangeException("Axis number must be between 1-9.");
			}

			ParentAddress = deviceAddress;
			Number = axisNumber;
			Port = port;
		}


		/// <summary>
		/// Create a new instance of the ZaberAsciiAxis class.
		/// </summary>
		/// <param name="parent">The parent device.</param>
		/// <param name="number">The axis number.</param>
		public ZaberAsciiAxis(ZaberAsciiDevice parent, int number)
		{
			if (null == parent)
			{
				throw new ArgumentNullException("An axis must have a parent device.");
			}

			if ((number < 1) || (number > 9))
			{
				throw new ArgumentOutOfRangeException("Axis number must be between 1-9.");
			}

			Port = parent.Port;
			ParentAddress = parent.Address;
			Number = number;
		}


		/// <summary>
		/// Homes the device.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void Home()
		{
			var homeCommand = new AsciiCommand(this, "home");
			Port.Write(homeCommand);
			Receive();
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device to the specified position.
		/// </summary>
		/// <param name="position">The position in microsteps.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveAbsolute(decimal position)
		{
			position = Math.Round(position);
			var moveAbsCommand = new AsciiCommand(this,
				"move abs " + position.ToString());
			Port.Write(moveAbsCommand);
			Receive();
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device by a certain distance.
		/// </summary>
		/// <param name="distance">The distance in microsteps.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveRelative(decimal distance)
		{
			distance = Math.Round(distance);
			var moveRelCommand = new AsciiCommand(this,
				"move rel " + distance.ToString());
			Port.Write(moveRelCommand);
			Receive();
			PollUntilIdle();
		}


		/// <summary>
		/// Moves the device at a certain speed.
		/// </summary>
		/// <remarks>
		/// This command differs from the other two move commands in that it
		/// returns immediately instead of waiting for the device to finish
		/// its movement and become idle. For this reason, the Stop() method
		/// is also provided.
		/// Note that if you have Alert messages enabled on your device, this
		/// command could result in unconsumed alerts waiting to be received.
		/// Call Port.Drain() before issuing the next command if this causes problems.
		/// </remarks>
		/// <param name="speed">The speed to move at.</param>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void MoveVelocity(decimal speed)
		{
			speed = Math.Round(speed);
			var moveVelCommand = new AsciiCommand(this,
				"move vel " + speed.ToString());
			Port.Write(moveVelCommand);
			Receive();
			// Do *not* poll until idle: allow move vel to be interrupted.
		}


		/// <summary>
		/// Stops the device.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void Stop()
		{
			var stopCommand = new AsciiCommand(this, "stop");
			Port.Write(stopCommand);
			Receive();
			PollUntilIdle();
		}


		/// <summary>
		/// Polls the device to determine whether it is currently moving.
		/// </summary>
		/// <returns>true if the device is busy; false if it is idle.</returns>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public bool IsBusy()
		{
			var pollStatusCommand = new AsciiCommand(this, "");
			Port.Write(pollStatusCommand);
			var statusReply = Receive();
			return statusReply.Busy;
		}


		/// <summary>
		/// Gets the position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		/// <remarks>This method is deprecated because 32-bit integers cannot
		/// represent the full range of positions on newer Zaber devices.
		/// Use <see cref="GetCurrentPosition()"/> instead.</remarks>
		[Obsolete("Use GetCurrentPosition() instead.")]
		public int GetPosition()
		{
			var getPosCommand = new AsciiCommand(this, "get pos");
			Port.Write(getPosCommand);
			var posReply = Receive();
			return int.Parse(posReply.ResponseData);
		}


		/// <summary>
		/// Gets the current position of the axis, in microsteps.
		/// </summary>
		/// <returns>The axis position.</returns>
		public decimal GetCurrentPosition()
		{
			var getPosCommand = new AsciiCommand(this, "get pos");
			Port.Write(getPosCommand);
			var posReply = Receive();
			return decimal.Parse(posReply.ResponseData);
		}


		/// <summary>
		/// Blocks while Polling the device at a regular interval until it
		/// reports itself as idle.
		/// </summary>
		/// <exception cref="UnexpectedReplyReceivedException">An unexpected
		/// reply was read from the serial port.</exception>
		public void PollUntilIdle()
		{
			while (IsBusy())
			{
			}
		}

		#region private helper functions

		private AsciiReply Receive()
		{
			var reply = Port.Read() as AsciiReply;
			if (null == reply)
			{
				throw new UnexpectedReplyReceivedException(
					"Could not parse received data as an ASCII reply.");
			}

			if ((reply.DeviceAddress != ParentAddress) || (reply.AxisNumber != Number))
			{
				throw new UnexpectedReplyReceivedException(
					string.Format("An unexpected reply was read "
					+ "from device {0}, axis {1}.", reply.DeviceAddress,
					reply.AxisNumber));
			}

			if (reply.Rejected)
			{
				throw new UnexpectedReplyReceivedException(
					string.Format("A command was rejected. "
					+ "Reported reason: {0}.", reply.ResponseData));
			}

			return reply;
		}

		#endregion
	}
}
