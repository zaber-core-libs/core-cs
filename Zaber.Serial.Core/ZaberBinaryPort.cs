﻿using System;
using System.Collections.Generic;
using System.IO.Ports;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// Represents a port connected to devices using the binary protocol.
	/// </summary>
	public class ZaberBinaryPort : IZaberPort, IDisposable
	{
		/// <summary>
		/// Indicates that no time-out should occur.
		/// </summary>
		public const int InfiniteTimeout = SerialPort.InfiniteTimeout;


		/// <summary>
		/// Initializes a new instance of the ZaberBinaryPort class using the
		/// specified port name.
		/// </summary>
		/// <param name="portName">The port to use (for example, COM1).</param>
		/// <param name="baudRate">Optional baud rate to use; defaults to 9600.</param>
		/// <exception cref="System.IO.IOException">The specified port could not be
		/// found or opened.</exception>
		/// <exception cref="ArgumentException">The specified baud rate is not supported.
		/// </exception>
		public ZaberBinaryPort(string portName, int baudRate = 9600)
		{
			if (null == portName)
			{
				throw new ArgumentNullException("portName cannot be null.");
			}

			if (baudRate != 115200 // short-circuit to the most common
				&& baudRate != 9600
				&& baudRate != 19200
				&& baudRate != 38400
				&& baudRate != 57600)
			{
				throw new ArgumentException("Valid baud rates are 9600, "
					+ "19200, 38400, 57600, and 115200.");
			}

			_port = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
		}


		#region -- IZaberPort implementation --

		/// <summary>
		/// The name of the port.
		/// </summary>
		public string PortName
		{
			get { return _port.PortName; }
			private set
			{
				_port.PortName = value;
			}
		}


		/// <summary>
		/// Whether the port is open.
		/// </summary>
		public bool IsOpen
		{
			get { return _port.IsOpen; }
		}


		/// <summary>
		/// The number of milliseconds before a time-out occurrs when a read
		/// operation does not finish.
		/// </summary>
		/// <remarks>
		/// This property allows you to set the read time-out value.
		/// The time-out can be set to any value greater than zero, or set to
		/// -1, in which case no time-out occurrs. -1 is the default.
		/// </remarks>
		public int ReadTimeout
		{
			get { return _port.ReadTimeout; }
			set 
			{
				if (value == -1)
				{
					// SerialPort.InfiniteTimeout *is* -1, but that's an 
					// implementation detail we can't rely on.
					_port.ReadTimeout = SerialPort.InfiniteTimeout;
				}
				else
				{
					_port.ReadTimeout = value;
				}
			}
		}


		/// <summary>
		/// Opens a new serial port connection.
		/// </summary>
		/// <remarks>
		/// This method closes and re-opens the port if the port is already
		/// open.
		/// </remarks>
		/// <exception cref="UnauthorizedAccessException">Access is denied to
		/// the port, or the current process or another process on the system
		/// already has the specified COM port open.</exception>
		/// <exception cref="System.IO.IOException">An attempt to open the port failed.
		/// Often this is caused by a port name being incorrect, or a port
		/// being disconnected.</exception>
		public void Open()
		{
			if (IsOpen)
			{
				Close();
			}

			_port.Open();
		}


		/// <summary>
		/// Closes the underlying SerialPort object's connection.
		/// </summary>
		/// <remarks>
		/// This method will only try to close the port if it is open.
		/// Attempting to close an already-closed port will do nothing.
		/// </remarks>
		public void Close()
		{
			if (IsOpen)
			{
				_port.Close();
			}
		}


		/// <summary>
		/// Sends a command to the port's devices.
		/// </summary>
		/// <param name="command">The command to be sent.</param>
		/// <exception cref="ArgumentNullException">The 
		/// <paramref name="command"/> passed is null.</exception>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Write(Command command)
		{
			var cmd = command as BinaryCommand;
			if (null == cmd)
			{
				throw new ArgumentNullException("command can not be null and must be a BinaryCommand.");
			}

			_port.Write(cmd.ToByteArray(), 0, 6);
		}


		/// <summary>
		/// Reads a reply from a device.
		/// </summary>
		/// <returns>The reply read.</returns>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		/// <exception cref="TimeoutException">No bytes were available to
		/// read.</exception>
		Reply IZaberPort.Read()
		{
			byte[] reply = new byte[6];
			for (int i = 6; i != 0; i -= _port.Read(reply, 6 - i, i))
			{
			}

			return new BinaryReply(reply);
		}


		/// <summary>
		/// Purges all input waiting on the input buffer, and attemps to drain
		/// all incoming data before returning.
		/// </summary>
		/// <remarks>
		/// This method will block until there is a 100ms period of silence in
		/// serial communication, guaranteeing that the next time Read() is
		/// called, it will receive a full message sent by the device some
		/// time after the call to Drain().
		/// </remarks>
		/// <remarks>
		/// This method is intended to be used when many commands have been
		/// sent without receiving any replies. If the user must suddenly read
		/// a reply to a certain command and has not been receiving replies
		/// otherwise, this method will allow them to immediately read the
		/// next incoming reply.
		/// </remarks>
		/// <remarks>
		/// Do not use this method in any setting where a serial line may
		/// never be quiet for over 100ms, as it may block forever. Some
		/// examples of such a setup are multiple devices chained together
		/// being polled constantly for position, or at least one device
		/// connected with move tracking enabled.
		/// </remarks>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Drain()
		{
			_port.DiscardInBuffer();

			int oldTimeout = ReadTimeout;
			ReadTimeout = 100;

			// Synchronously read until there's nothing for 100ms.
			while (true)
			{
				try
				{
					_port.ReadByte();
				}
				catch (TimeoutException)
				{
					break; // drained!
				}
			}

			ReadTimeout = oldTimeout;
		}


		/// <summary>
		/// Queries the port to find all connected axes.
		/// </summary>
		/// <remarks>
		/// This method will only find axes which respond at the specified
		/// baud rate and protocol.
		/// </remarks>
		/// <returns>A list of connected axes.</returns>
		public List<IZaberAxis> FindAxes()
		{
			var axes = new List<IZaberAxis>();
			var pollCommand = new BinaryCommand(0, 55, -1);
			Write(pollCommand);

			int oldTimeout = ReadTimeout;
			ReadTimeout = 50;

			while (true)
			{
				try
				{
					var reply = Read();
					axes.Add(new ZaberBinaryDevice(this, reply.DeviceAddress));
				}
				catch (TimeoutException)
				{
					break; // Found all devices.
				}
			}

			ReadTimeout = oldTimeout;

			return axes;
		}

		#endregion

		/// <summary>
		/// Writes an array of bytes to the port.
		/// </summary>
		/// <remarks>
		/// This method will always write the first six bytes of 
		/// <paramref name="command"/> to the port. In order to behave as
		/// intended, the last four bytes must encode the desired data in a
		/// little-endian representation, as specified by %Zaber's Binary
		/// Protocol Manual.
		/// </remarks>
		/// <param name="command">An array of "raw" bytes to be sent.</param>
		/// <exception cref="ArgumentNullException">The 
		/// <paramref name="command"/> passed is null.</exception>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		public void Write(byte[] command)
		{
			if (null == command)
			{
				throw new ArgumentNullException("command cannot be null.");
			}

			_port.Write(command, 0, 6);
		}


		/// <summary>
		/// Reads a reply from a device.
		/// </summary>
		/// <returns>The reply read.</returns>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		/// <exception cref="TimeoutException">No bytes were available to
		/// read.</exception>
		public BinaryReply Read()
		{
			byte[] reply = new byte[6];
			for (int i = 6; i != 0; i -= _port.Read(reply, 6 - i, i))
			{
			}

			return new BinaryReply(reply);
		}


		/// <summary>
		/// Releases the resources used by the underlying SerialPort object.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}


		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				_port.Dispose();
			}
		}


		private SerialPort _port;
	}
}
