﻿using System;
using System.Collections.Generic;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// An interface for communicating with %Zaber devices over a serial port.
	/// </summary>
	/// <remarks>
	/// Objects implementing IZaberPort can be thought of as extensions which 
	/// simplify the built-in System.IO.Ports.SerialPort class. They are
	/// configured specifically for communicating with %Zaber devices. They
	/// are designed to interact primarily with "Command" and "Reply" objects
	/// instead of raw strings and byte arrays, though one can work with this
	/// class entirely in terms of "string" and "byte[]" if one so wishes.
	/// </remarks>
	public interface IZaberPort : IDisposable
	{
		/// <summary>
		/// The name of the port.
		/// </summary>
		string PortName { get; }


		/// <summary>
		/// Whether the port is open.
		/// </summary>
		bool IsOpen { get; }


		/// <summary>
		/// The number of milliseconds before a time-out occurrs when a read
		/// operation does not finish.
		/// </summary>
		/// <remarks>
		/// This property allows you to set the read time-out value.
		/// The time-out can be set to any value greater than zero, or set to
		/// -1, in which case no time-out occurrs. -1 is the default.
		/// </remarks>
		int ReadTimeout { get; set; }


		/// <summary>
		/// Opens a new serial port connection.
		/// </summary>
		/// <remarks>
		/// This method closes and re-opens the port if the port is already
		/// open.
		/// </remarks>
		/// <exception cref="UnauthorizedAccessException">Access is denied to
		/// the port, or the current process or another process on the system
		/// already has the specified COM port open.</exception>
		/// <exception cref="System.IO.IOException">An attempt to open the port failed.
		/// Often this is caused by a port name being incorrect, or a port
		/// being disconnected.</exception>
		void Open();


		/// <summary>
		/// Closes the underlying SerialPort object's connection.
		/// </summary>
		/// <remarks>
		/// This method will only try to close the port if it is open.
		/// Attempting to close an already-closed port will do nothing.
		/// </remarks>
		void Close();


		/// <summary>
		/// Sends a command to the port's devices.
		/// </summary>
		/// <param name="command">The command to be sent.</param>
		/// <exception cref="ArgumentNullException">The 
		/// <paramref name="command"/> passed is null or the wrong type for the 
		/// protocol.</exception>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		void Write(Command command);


		/// <summary>
		/// Reads a reply from a device.
		/// </summary>
		/// <returns>The reply read.</returns>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		/// <exception cref="TimeoutException">No bytes were available to
		/// read.</exception>
		Reply Read();


		/// <summary>
		/// Purges all input waiting on the input buffer, and attemps to drain
		/// all incoming data before returning.
		/// </summary>
		/// <remarks>
		/// This method will block until there is a 100ms period of silence in
		/// serial communication, guaranteeing that the next time Read() is
		/// called, it will receive a full message sent by the device some
		/// time after the call to Drain().
		/// </remarks>
		/// <remarks>
		/// This method is intended to be used when many commands have been
		/// sent without receiving any replies. If the user must suddenly read
		/// a reply to a certain command and has not been receiving replies
		/// otherwise, this method will allow them to immediately read the
		/// next incoming reply.
		/// </remarks>
		/// <remarks>
		/// Do not use this method in any setting where a serial line may
		/// never be quiet for over 100ms, as it may block forever. Some
		/// examples of such a setup are multiple devices chained together
		/// being polled constantly for position, or at least one device
		/// connected with move tracking enabled.
		/// </remarks>
		/// <exception cref="InvalidOperationException">The specified port is
		/// not open.</exception>
		void Drain();


		/// <summary>
		/// Queries the port to find all connected axes.
		/// </summary>
		/// <remarks>
		/// This method will only find axes which respond at the specified
		/// baud rate and protocol.
		/// </remarks>
		/// <returns>A list of connected axes.</returns>
		List<IZaberAxis> FindAxes();
	}
}
