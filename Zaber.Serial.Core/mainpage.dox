namespace Zaber.Serial.Core {
/**
\mainpage The %Zaber %Core %Serial Library in C#

\tableofcontents

\attention This library is deprecated and is no longer maintained or supported.
The modern alternative is the
[Zaber Motion Library](https://software.zaber.com/motion-library/docs).

The %Zaber %Core %Serial Library is intended to provide a simple set of commands
for interacting with %Zaber devices over a serial port. It is recommended that
before using this library, you first read the protocol manual corresponding to
the protocol of your device.

The [ASCII Protocol Manual]
(http://www.zaber.com/wiki/Manuals/ASCII_Protocol_Manual) is a complete
reference for communicating with %Zaber devices in ASCII. Note that many
A-Series devices default to the binary protocol, and may need to be switched to
ASCII before use. [Appendix C](http://www.zaber.com/wiki/Manuals/AS
CII_Protocol_Manual#Appendix_C_-_Switching_between_Binary_and_ASCII_Protocols)
of the ASCII Protocol Manual is a short guide to doing this switch.

The [Binary Protocol Manual](http://www.zaber.com/wiki/Manuals/Binary_Protocol_
Manual) is a complete reference for communicating with %Zaber devices in binary.
Note that devices using the binary protocol will only reply once a command has
completed, so more work must be done to properly receive responses. See the
["Move Absolute" command reference](http://www.zaber.com/wiki/Manuals/Binary_Pr
otocol_Manual#Move_Absolute_-_Cmd_20) for more info on this.

\section downloading Downloading
The officially published binary versions of the library can selected and
downloaded [here](https://api.zaber.io/downloads/software-finder?product=core_csharp)
(select the version you want then click the button),
or you can obtain the source code and build it yourself from the
[public repo](https://gitlab.com/zaber-core-libs/core-cs).

\section gettingStarted Getting Started
This library, at its core, is made up of four interfaces and abstract classes:
IZaberPort, IZaberAxis, 
Command, and Reply. 

Classes implementing IZaberPort will look suspiciously like the built-in 
[System.IO.Ports.SerialPort](http://msdn.microsoft.com/en-us/library/system.io
.ports.serialport.aspx) class. This is intentional: the IZaberPort
interface is intended as a simplifying layer built on top of the SerialPort
class. Though IZaberPort does not define Read and Write methods, both of its
implementing classes do. These classes are ZaberAsciiPort,
and ZaberBinaryPort. Choose the one which matches your
devices' communication protocol. Note that the default read time-out is
infinite, just like that of SerialPort. This can be changed using the
IZaberPort.ReadTimeout property.

The IZaberAxis interface adds an extra layer of convenient abstraction on top
of IZaberPort, meant to make communicating with %Zaber devices easier by 
modelling individual device axes. IZaberAxis does impose some limitations on
the programmer, the most fundamental of which being that it only provides
blocking methods which return when the axis has completed its movement. If you
want to move multiple devices at once, classes implementing IZaberAxis will be
of limited use. There are two classes which implement IZaberAxis,
ZaberAsciiAxis, and ZaberBinaryDevice. 
A third class, ZaberAsciiDevice, exists to connect 
ZaberAsciiAxis objects to their parent ZaberAsciiPort objects. 

Subclasses of Command and Reply are also divided by protocol. They model
individual commands and replies to be sent to and received from devices or
axes. They are used extensively in the library internally, and they provide
methods for encoding commands for serial transmission and for decoding
received replies to provide programmatic access to their data.

\section mpExamples Examples

See \link examples the "Examples" page \endlink for examples of how to use
this library.

\section knownIssues Known Issues
The built-in SerialPort class has an issue where if a SerialPort object is
used to open a COM port, then closed, and then opened again too soon, it will
still be
unavailable. This will throw an exception. Microsoft's documentation mentions
this in the ["Remarks" section](http://msdn.microsoft.com/en-us/library/system
.io.ports.serialport.open.aspx#remarksToggle) of SerialPort.Open() and 
SerialPort.Close(): 

<blockquote><i>Only one open connection can exist per 
SerialPort object. The best practice for any application is to wait for some
amount of time after calling the Close method before attempting to call the
Open method, as the port may not be closed instantly.</i></blockquote>

This problem 
persists even if the SerialPort's Dispose() method is called, though this does
help the problem to some degree. The problem gets noticeably worse when trying
to access one COM port from multiple SerialPort objects in quick succession.
Since this library uses SerialPort as a base for serial communications, it
suffers from the same problem.

\section unsupportedFeatures Unsupported Features
Some features of the %Zaber firmware are incompatible with this library. The
library is designed to work with devices set to the default settings,
so do not worry if you have not changed any settings on your device.
Message IDs and checksums are not supported by the library in either protocol.

ZaberBinaryDevice will only work when the binary device does not have
[Auto-Reply](http://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual#Set
_Auto-Reply_Disabled_Mode_-_Cmd_101) disabled. This is the default setting.
In addition, ZaberBinaryDevice does some reply validation and will ignore
automatic responses related to move tracking, but we also recommend that both the
[Move Tracking](http://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual#Set_M
ove_Tracking_Mode_-_Cmd_115) and [Manual Move Tracking]
(http://www.zaber.com/wiki/Manuals/Binary_Protocol_Manual#Set_Manual_Move_Trac
king_Disabled_Mode_-_Cmd_116) settings be disabled to ensure more predictable
behaviour when using the ZaberBinaryDevice class.
  
*/
}