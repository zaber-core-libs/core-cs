﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// An abstract class for commands of all forms to inherit from.
	/// </summary>
	public abstract class Command : IEquatable<Command>
	{
		/// <summary>
		/// The address of the device to which to send a command, or 0 to send
		/// a command to all devices on the chain.
		/// </summary>
		public byte DeviceAddress;


		/// <summary>
		/// Creates a new instance of the Command class.
		/// </summary>
		protected Command()
		{
		}


		/// <summary>
		/// Returns a hash code based off the command's attributes.
		/// </summary>
		/// <remarks>
		/// The hash code returned by this function depends on all of the
		/// attributes of the object, including the mutable ones. This means
		/// that when using this class in a HashMap or similar structure, it
		/// should be considered immutable, or else it will be lost upon being
		/// changed.</remarks>
		/// <returns>The calculated hash code.</returns>
		public abstract override int GetHashCode();


		/// <summary>
		/// Returns a human-readable representation of this command.
		/// </summary>
		/// <returns>A string representing the command.</returns>
		public abstract override string ToString();


		/// <summary>
		/// Tests the equality of any object against this command.
		/// </summary>
		/// <param name="obj">The object to be compared.</param>
		/// <returns>True if the object and this command are equal,
		/// false if they are inequal.</returns>
		public override bool Equals(object obj)
		{
			Command command = obj as Command;

			if (null == obj)
			{
				return false;
			}

			return Equals(command);
		}


		/// <summary>
		/// Checks to see if this command is equal to another Command.
		/// </summary>
		/// <param name="command">The command to compare this one to.</param>
		/// <returns>True if the commands are equal, false if inequal.</returns>
		public abstract bool Equals(Command command);
	}
}
