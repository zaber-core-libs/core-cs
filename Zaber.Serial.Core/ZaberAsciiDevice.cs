﻿using System;

namespace Zaber.Serial.Core
{
	/// <summary>
	/// A class to represent a single- or multi-axis %Zaber device using the
	/// ASCII protocol.
	/// </summary>
	/// <remarks>
	/// <see cref="ZaberAsciiAxis"/> is the intended class for controlling
	/// devices using the ASCII protocol. This encourages thinking of devices
	/// in terms of axes instead of whole devices, and helps to unify the
	/// ASCII and binary halves of the library.
	/// 
	/// Consider using this class to control a multi-axis device like so:
	/// <code>
	/// var device = new ZaberAsciiDevice(myPort, 1);
	/// 
	/// device.GetAxis(1).Home();
	/// device.GetAxis(2).MoveRelative(1000);
	/// device.GetAxis(1).MoveAbsolute(2000);
	/// </code>
	/// </remarks>
	public class ZaberAsciiDevice
	{
		/// <summary>
		/// The parent port of the device.
		/// </summary>
		public IZaberPort Port { get; private set; }


		/// <summary>
		/// The device address.
		/// </summary>
		public byte Address;


		/// <summary>
		/// Create a new instance of the ZaberAsciiDevice class.
		/// </summary>
		/// <param name="port">The port the device is connected to.</param>
		/// <param name="address">The device address.</param>
		/// <exception cref="ArgumentNullException"><paramref name="port"/> is
		/// null.</exception>
		public ZaberAsciiDevice(IZaberPort port, byte address)
		{
			if (null == port)
			{
				throw new ArgumentNullException("port cannot be null.");
			}

			Port = port;
			Address = address;
		}


		/// <summary>
		/// Get the axis with the specified number.
		/// </summary>
		/// <remarks>
		/// This method will create the axis specified and assume that the
		/// device does indeed have such an axis. It is up to you to only make
		/// axes which actually exist on the device.
		/// 
		/// For a method which will detect the number of axes a device has,
		/// see ZaberAsciiPort.FindAxes(), or send the "get system.axiscount"
		/// ASCII command directly to a device.
		/// </remarks>
		/// <param name="number">The number of the axis.</param>
		/// <returns>A matching ZaberAsciiAxis instance.</returns>
		/// <exception cref="ArgumentException"><paramref name="number"/> is
		/// not between 1 and 9.</exception>
		public ZaberAsciiAxis GetAxis(int number)
		{
			if ((number < 1) || (number > 9))
			{
				throw new ArgumentException("Axis number must be 1-9.");
			}

			return new ZaberAsciiAxis(this, number);
		}
	}
}
